%!TEX root = yoshi.tex
\begin{table}[!t]
		\centering
		\tiny
		\caption{Characteristics of the Software Projects Considered in the Study, as extracted from GitHub on April 2017 - Domain Taxonomy tailored from literature.}
		\label{tab:systems}
			%\rowcolors{2}{gray!25}{white}
			\begin{tabular}{lcccccc}\toprule
				Name & \# Rel. & \# Commits & \# Members & \# Language & \#KLOC & Domain \\ \midrule
			
				Netty & 164 & 8,123 & 258 & JavaScript & 438 & Software Tools\\
				Android & 3 & 132 & 14 & Java & 382 & Library \\
				Arduino & 74 & 6,516 & 210 & C & 192 & Rapid prototyping\\
				Bootstrap & 55 & 2,067 & 389 & JavaScript & 378 & Web libraries and fw.\\
				Boto & 86 & 7,111 & 495 & Python & 56 & Web libraries and fw.\\
				Bundler & 251 & 8,464 & 549 & Java & 112 & Web libraries and fw.\\
				Cloud9 & 97 & 9,485 & 64 & ShellScript & 293 & Application software\\
				Composer & 35 & 7,363 & 629 & PHP & 254 & Software Tools\\
				Cucumber & 8 & 566 & 15 & Java & 382 & Software Tools\\
				Ember-JS & 129 & 5,151 & 407 & JavaScript & 272 & Web libraries and fw.\\
				Gollum & 76 & 1,921 & 143 & Gollum & 182 & App. fw.\\
				Hammer & 25 & 1,193 & 84 & C\# & 199 & Web libraries and fw.\\
				BoilerPlate & 12 & 469 & 48 & PHP & 266 & Web libraries and fw.\\
				Heroku & 52 & 353 & 10 & Ruby & 292 & Software Tools\\
				Modernizr & 27 & 2,392 & 220 & JavaScript & 382 & Web libraries and fw.\\
				Mongoid & 253 & 6,223 & 317 & Ruby & 187 & App. fw.\\
				Monodroid & 2 & 1,462 & 61 & C\# & 391 & App. fw.\\
				PDF-JS & 43 & 9,663 & 228 & JavaScript & 398 & Web libraries and fw.\\
				Scrapy & 78 & 6,315 & 242 & Python & 287 & App. fw.\\
				Refinery & 162 & 9,886 & 385 & JavaScript & 188 & Software Tools\\
				Salt & 146 & 81,143 & 1,781 & Python & 278 & Software Tools\\
				Scikit-Learn & 2 & 4,456 & 17 & Python & 344 & App. and fw.\\
				SimpleCV & 5 & 2,625 & 69 & Python & 389 & App. and fw.\\
				Hawkthorne & 116 & 5,537 & 62 & Lua & 211 & Software Tools\\
				SocketRocket & 10 & 494 & 67 & Obj-C & 198 & App. fw.\\\bottomrule
				
			\end{tabular}
	\end{table}	
			

	The \emph{goal} of the study is to evaluate the ability of \yoshi to discriminate the different community types in open source, with the \emph{purpose} of understanding to what extent the proposed method can be adopted to analyse the social relationships occurring among the developers of a software system. 
	To achieve this goal, we explore two main research questions aimed at (i) evaluating the accuracy of the measurements provided by \yoshi and (ii) evaluating the potential usefulness of the tool in practice:
	
	\begin{itemize}
				
		\item \textbf{RQ$_1$.} \emph{Does \yoshi correctly measure the community aspects characterising different software communities?}\smallskip
					
		\item \textbf{RQ$_2$.} \emph{Does \yoshi provide a correct indication of the community structure of a software system?}
				
	\end{itemize}	
	
	The \emph{context} of the study consists of 25 open source software communities coming from the GitHub repository, sampled according to guidelines from the state of the art \citep{FalessiSS17} and refining our results using best-practice sampling criteria \citep{KalliamvakouGBS16}. 
	Table \ref{tab:systems} reports the characteristics of the subject projects\footnote{Characteristics extracted on April 2017} in terms of (i) their size measured as number of public releases issued and number of commits performed over their history, (ii) number of contributors who committed at least once to the repository, and (iii) their application domain according to the taxonomy proposed by \citet{borgesICSME}.
	To select this dataset, we first ranked the GitHub projects based on number of commits, to control for project activity; in this respect, a fixed boundary of no less than 100 commits was adopted. Then, projects were filtered based on number of members (at least 10) and number of LOCs (at least 50k): in this way, we allowed the selection of non-trivial communities that have to deal with large codebases. Moreover, we also based our selection on diversity: in cases where two projects had the same scope, we randomly excluded one of them in order to pick a population that was as different as possible (note that the domain might still be the same, as it refers to the general objective of a certain project~\citep{borgesICSME}). Finally, we have manually inspected the selected projects and made sure that all of them are real projects (rather than student projects, assignments, etc.), as suggested by recent work \citep{munaiah2017curating}.
	The specific query employed for the selection of the subject projects was done on April 2017 and can be found in our on-line appendix \citep{tsc:appendix}.\medskip
	
	To answer our first research question, we evaluated whether the metrics computed by our approach actually represent valid community measurements: indeed, a necessary condition to provide automated support for community steering and governance is that \yoshi delivers reliable insights into key community characteristics and type. 
	To this aim, it is \emph{necessary and sufficient} that the metrics coded within \yoshi satisfy the representation condition \citep{repcond}, given that the decision-tree algorithm within YOSHI only represents a partial-order function among said community characteristics identified by the metrics. 
	According to \citet{repcond}, the representation condition for a metric holds ``\emph{if and only if a measurement mapping maps the entities into numbers, and empirical relations into numerical relations in such a way that the empirical relations are preserved by the numerical relations}". 
	This means that, for instance, paraphrasing from \citet{repcond}: ``if we have an intuitive understanding that A is taller than B, then also the measurement mapping M must give that M(A) $>$ M (B). The other way around, if M(A) $>$ M (B) then it must be that A is intuitively understood to be taller than B". 
	In our work, we ran \yoshi on the subject systems in our dataset and then, for each metric computed by the approach, we evaluated the representation condition using the guidelines provided by \citet{repcond}.\smallskip
		
	In order to answer \textbf{RQ$_2$}, we conducted a validation aimed at verifying the quality of the community structure extracted by \textsc{Yoshi}. As explained in Section \ref{subsec:communityStructure}, this is the main characteristic that leads to the identification of a community type, and its validation provides insights into the meaningfulness of the operations performed by our tool \citep{titans}.
	To evaluate this aspect, we firstly extracted the information about the actual community structure of the communities considered. As ground truth we exploited the OpenHub community\footnote{https://www.openhub.net/}, which reports data about different aspects of software communities, including a community structure modularity indicator comprised in the set $\lbrace LOW, MEDIUM, HIGH \rbrace$. It is worth noting that such data is \textbf{not} computed automatically but rather manually retrieved by the owners of OpenHub without the usage of proxy metrics, as directly reported in the OpenHub blog \citep{blogPost} as well as previous literature in the field \citep{chelkowski2016inequalities,druskat2016proposal}. While we cannot speculate on how the owners of OpenHub manually classify communities based on their community structure, it is important to note that this data is constantly validated by the community around the platform, thus allowing us to be confident about its actual validity.
			
	In the second place, we compared the social interactions detected by \yoshi with the ones identified by a \emph{null-model} \citep{Cohen:1988}, i.e., a network which matches the original network in some of its topological features, but which does not display community structure. Using this strategy, we were able to verify whether the graph built by \textsc{Yoshi} actually displays a community structure or whether it is no better than a randomly created graph.
	More specifically, in our context we compared our approach with the null-model proposed by \citet{NewGir04}, i.e., a randomized version of the original graph, where edges are rewired at random, under the constraint that the expected degree of each vertex matches the degree of the vertex in the original graph. 
	The comparison was made in terms of \emph{modularity} coefficients \citep{newman2006modularity}, i.e., an indicator that measures how well a network can be divided into clearly defined subsets of nodes. 
	The higher the value of the metric the higher the community structure is supposed to be. Similarly, the lower the value, the lower the estimated community structure. 
	
	It is important to note that to adequately compare the modularity structure output by both \yoshi and the null model with the actual community structure of a certain community, we needed to transform the numeric indexes in a nominal scale comprised in the set $\lbrace LOW, MEDIUM, HIGH \rbrace$. To this aim, we followed the guidelines by \citet{newman2006modularity}: the community structure is \emph{low} when $modularity < 0.30$, medium when $0.30 \le modularity < 0.41$, and high when $modularity \ge 0.41$.% \citep{newman2006modularity}. 
	Thus, if the modularity coefficient estimated by one (or both) of the experimented approaches is in accordance with the actual community structure modularity, then the approach correctly provides the indication.
	
	To statistically verify the results, we applied the paired Mann-Whitney test \citep{Conover:1998} comparing the distribution of the modularity coefficients computed by our approach with the ones computed by a randomly created one over the 25 considered systems. This is a non-parametric test used to evaluate the null hypothesis stating that it is equally likely that a randomly selected value from one sample will be less than or greater than a randomly selected value from a second sample. The results are intended as statistically significant at $\alpha = 0.05$. Note that in this case we relied on the numeric modularity values because we were interested in evaluating whether the indexed outputs by \yoshi were statistically different from those extracted by the random model.
	
	While the analysis of how \yoshi performs when compared with a null model might provide insightful hints on the value of the information extracted by the proposed approach, it is also important to note that such information should effectively assist the members of a certain community. In other words, the fact that \yoshi provides better information than a random model does not directly imply that it is actually useful for community members. Thus, we needed an additional validation that was designed to gather opinions on the quality of the information provided by \yoshi from the members of the considered communities.
		
	To this aim, we contacted the developers of the 25 open source communities considered: we limited our analyses to those developers having the highest number of commits (i.e., the most active ones), as they might have a more comprehensive knowledge of the development activities within the community and, therefore, a better overview of the underlying community structure that allow us to gather authoritative responses. On the contrary, we filtered out developers having a low number of commits and that are likely not so much involved in the community. 
	Therefore, we contacted via e-mail the developers having a number of commits higher than the third quartile of all commits performed on each of the considered systems, i.e., those contributing the most to each repository, and we asked them to comment about the community structure that was in place in the specific time period analyzed in our paper: to ease the task, we provided them with a spreadsheet containing three tabs: (i) the first reporting detailed information on the commits performed on the repository; (ii) the second with the developers taking part in the development process in the considered time window, and (iii) the list of all communications between developers during the time period. In this way, the developers could better remember the project status in the period, and provide us with more careful observations of the community structure taking place in that period. To further ease the task, we allowed developers to give us open answers, i.e., we did not provide them with fixed check-boxes reporting the possible community structures. We decided to go for this solution as developers might be not aware of the formal definition of the underlying community structure of their project.

	When analyzing the developers' answers, we proceeded with a manual match of their opinions to the automatic community structure assigned by \yoshi to the community a certain developer corresponded to. To avoid any kind of confirmation bias, we recruited two independent external developers having more than 5 years of programming experience (from now on, we refer to them as the \emph{inspectors}) and asked them to independently perform such a mapping. Specifically, we provided the two inspectors with the developers' answers and a list composed of the community types extractable using \textsc{Yoshi}. The task was to analyze each answer and tag it with one or more community types. For instance, if a developer replied by saying that \emph{``in the considered time period, all communications passed from the mediation of one member, who had the role of disseminating it to other developers''}, the inspectors mapped this answer to the definition of formal community. This process required approximately 1.5 hours. At the end, we first computed the inter-rater agreement between the two inspectors using the Krippendorff’s alpha Kr$_\alpha$ \citep{krippendorff04}. Agreement measures to 0.90, considerably higher than the 0.80 standard reference score for Kr$_\alpha$~\citep{AntoineVL14} . In cases of disagreement, the inspectors opened a discussion in order to find a joint solution.
	In the second place, we verified how many times \yoshi was able to properly identify the community structure perceived by the developers of the considered project. 
	
	\smallskip
	
	%To answer \textbf{RQ3} and evaluate the relationship between community types and community smells we performed a release-level analysis aimed at investigating how the four smell types taken into account, i.e., \emph{the Organisational Silo Effect}, \emph{the Black-cloud Effect}, \emph{the Lone-wolf Effect}, and \emph{the Bottleneck or ``Radio-silence" Effect}, relate to the particular community types observed during the history. Note that we performed such an evolutionary analysis because in this way we could have a much richer and meaningful dataset if compared to the only 25 data points represented by the most recent snapshots of the communities considered. However, due to computational reasons, we needed to limit the analysis to the 220 releases belonging to two projects (the biggest) of our dataset, i.e., \textsc{Arduino} and \textsc{Salt}. We selected these specific two systems to ensure that our observations covered all the community types \yoshi can identify. Indeed, it is important to remark that a certain community can be associated with more community types (see Section \ref{bg}): during the releases of the \textsc{Arduino} and \textsc{Salt} we found that different community types emerged, allowing us to effectively evaluate the relationship between the presence of smells and the existence of a certain community structure.
	
	%To detect the community smells, we exploited a fork of the \textsc{CodeFace} tool \cite{Joblin:2015} defined in previous and related work \cite{archcomm,Tamburri:icsa}. On one hand, CodeFace is able to elicit and allow the study of \emph{verified communities} \cite{Joblin:2015}, that is, communities that reflect the ground-truth community structure. On the other hand, our fork of CodeFace augments the standard detection rules in order to detect the four community smells taken into account. Once we have extracted the list of community smells contained in each of the releases of the projects considered, we mined association rules \cite{Agrawal:1993} for detecting which community types and smells co-occur. We applied the \textsc{aPriori} algorithm \cite{Agrawal:1993} on the dataset containing all the types and communities retrieved in the considered systems. In Section \ref{sec:results} we report and discuss the association rules having a support higher than 0.6 and confidence  higher than 0.8 \cite{Agrawal:1993}. This focus is necessary to produce and report only the association rules having the highest strength. Furthermore, we computed the lift metric, which measures the ability of a rule to correctly identify a relationship with respect to a random choice model \cite{Agrawal:1993}. A lift value higher than 1 indicates that the left-hand and right-hand operators of an association rule appear together more often than expected, thus meaning that the occurrence of the left-hand operator often implies the co-presence of the right-hand operator. To understand the statistically significance of the rules found, we employed Fisher's exact test \cite{Fisher} on the lift value achieved by the mined association rules: specifically, the test measures the significance of the deviation between the association rule model and the random choice models compared when computing the lift. The statistical significance is obtained in case of p-value lower than 0.05. \smallskip
%In particular, association rule mining is an unsupervised learning technique able to discover hidden relationships in large datasets.
	
	It is worth remarking that all the data and scripts used to evaluate \textsc{Yoshi} are publicly available in the form of a replication package in our on-line appendix \citep{tsc:appendix}.
	
%	\FABIO{Final remark: in Section 2, we explicitly mention previous work done in the context of industrial communities. In the discussion of the results, we should perform a point-to-point comparison with such previous findings.}
	
	
	