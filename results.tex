%!TEX root = yoshi.tex
\begin{table}[!t]
	\centering
	\tiny
	\caption{Community types inferred by \textsc{Yoshi} for the considered software projects. As explained in Section 3, more community types can be associated to a single project.}
	\label{tab:types}
	%\rowcolors{2}{gray!25}{white}
	\begin{tabular}{lc}
		\toprule
		Project & Community Type(s) \\ \midrule
		Netty & IC, FN, FG \\
		Android & IC, FN, FG \\
		Arduino & IC, FN, FG \\
		Bootstrap & IN, NOP \\
		Boto & IC, IN\\
		Bundler & NOP, FG \\
		Cloud9 & IC, FN, FG \\ 
		Composer & IC, FN, FG \\
		Cucumber & IN, IC, NOP \\
		Ember-JS & FN, FG, WG \\
		Gollum & IC, FN, FG \\
		Hammer & IN, NOP \\
		BoilerPlate & IN,NOP\\ 
		Heroku & NOP, IN, FG \\
		Modernizr & IN, NOP, WG \\
		Mongoid & FN, FG, WG \\
		Monodroid & IC, IN, FG \\
		PDF-JS & IN, NOP \\
		Scrapy & FN, FG, WG \\
		RefineryCMS & FG, WG \\
		Salt & FN, FG, WG \\
		Scikit-Learn & NOP, IN, FG \\
		SimpleCV & IC, NOP, IN, FG \\
		Hawkthorne & IC, IN, FG \\
		SocketRocket & NOP, IN, FG \\\bottomrule
		
	\end{tabular}
\end{table}	

Before discussing the results for the two research questions formulated in the previous section, Table \ref{tab:types} reports for each system in the dataset the associated community types as inferred by \textsc{Yoshi}. All the projects exhibit characteristics attributable to more than one community type. Interestingly, about 60\% of the communities have been typified as formal groups (FG) or formal networks (FN). This contrasts our expectation that open source projects are generally considered poorly formal, because of the contributions originating from volunteers. Yet, this evidence suggests to confirm theories reporting that a formal organisation is often needed to coordinate the activities among developers of a projects \citep{kraut1995coordination,elkins2003leadership}. This finding is also confirmed by anecdotal evidence we found around several of the communities in our sample. For instance, the Netty project provides a formal guide\footnote{https://netty.io/community.html} newcomers have to follow to be considered part of the community, thus establishing a formal structure and specific rules that community members must adhere to. More in line with the typical notion associated with open-source communities is the evident presence of Networks of Practice (around 40\% of our sample) followed by working-groups, which, by their tightly-knit nature, are associated with collocated communities such as Modernizr (a tight community project with devoted maintainers: \url{https://modernizr.com/}).

\subsubsection{Does \yoshi correctly measure the community aspects characterising different software communities?}
	The first step in establishing if a metric provides a correct value is the satisfaction of its representation condition \citep{repcond}. 
	Essentially, the representation condition states that a valid metric \emph{must prove} that the observed empirical relation between the measured attributes is preserved by their associated numerical relations. To provide an objective validation for \yoshi metrics we analysed them individually, evaluating whether their representation condition hold by means of our own visual inspection. In particular, for each metric considered by \textsc{Yoshi} we performed the following steps:
	
	\begin{enumerate}
		
		\item We compute the values of the metric $m$ for two communities $C_i$ and $C_j$ in our dataset;
		\item If $m(C_i) > m(C_j)$, then we verified that the metric value computed on the community $C_i$ was actually higher than the metric value computed on $C_j$.
		
	\end{enumerate}

	In this paper, we focus on showing the evaluation of the representation condition of the \emph{Geodispersion}, \emph{Engagement} and \emph{Formality} metrics. The proofs for remaining metrics  refer to literature, that is, Longevity and Cohesion are well-established social-networks analysis metrics \citep{EggheR03,Tikhonov2016Communitylevel,KozdobaM15}.
	
	\paragraph{\textbf{Geodispersion.}}
		\emph{Let us consider that the members of the community $C1$ are more dispersed (e.g., according to manual visual inspection) than the members of the community $C2$ if the average geo-dispersion across all the members of $C1$ is higher than the geo-dispersion across the members of $C2$. Considering the function average distance $AD$ which maps average distances between community members, we can say that $C1$ is more dispersed than $C2$ if and only if:} 
		
		\begin{equation}
			AD(C1) > AD(C2)
		\end{equation}

	\noindent where the average distance $AD$ is computed as follows:\medskip
	
	\emph{Let $A$ be a member of the community, and let $M_{a}$ be the average for geographical and cultural distances between $A$ and the rest of the community members. The distance between two members is defined by the spherical distance between the geographical coordinates of the two users, determined using the same measuring unit, i.e., kilometers. Similarly, the cultural distance can be obtained following the Hofstede metrics \citep{hofstede2010cultures}.}
	This operation is then applied for each member of the community, obtaining a set $M = \lbrace M_{a}, M_{b}, ..., M_{i}, ..., M_{n} \rbrace$ composed of the average geographical and cultural distances between each member $i$ and the rest of the community. Thus, the average distance $AD$ between community members is given by the average of the $M_i \in M$. 
		
	\begin{figure}
		\centering
		\includegraphics[scale=.7]{Figures/geodisp2.pdf}
		\caption{A geodistribution map for Twitter Bootstrap, the representation condition holds.}\label{geodisp2}
	\end{figure}

	\begin{figure}
		\centering
		\includegraphics[scale=.85]{Figures/geodisp1.pdf}
		\caption{A geodistribution map for Dotcloud.}\label{geodisp1}
	\end{figure}

	For sake of clarity, let's consider a practical case where the geo-dispersion of Twitter bootstrap\footnote{\url{https://github.com/twbs/bootstrap}} and dotcloud\footnote{\url{https://github.com/dotcloud/gitosis-on-dotcloud}} are compared. As reported on-line on the websites of projects, we know that (a) Twitter bootstrap has contributors from all over the world; (b) dotcloud has contributors that are mostly grouped around the company offices in California (USA).
	Figures \ref{geodisp2} and \ref{geodisp1} present the actual geographical distribution of members collaborating on the Twitter bootstrap and dotcloud projects, respectively, as evaluated by \textsc{Yoshi}. The red circles represent the location of community members\footnote{edges representing connections between users are not included in this figure, but a connection graph is available as part of the structural analysis}. 
	
	From a visual inspection, it is clear that the representation condition of the metric holds. From a numerical one, the geo-dispersion among the Twitter bootstrap community members computed by \yoshi is 6,221, while the one of dotcloud community members is 380. Based on the data computed by the application we visually confirmed that $AD(bootstrap)$ $>>$ $AD(dotcloud)$, which means that members of the Twitter bootstrap community are more dispersed than the members of the dotcloud project. Thus, we conclude that the representation condition for the \emph{Geodispersion} metric is proven.
	
	\paragraph{\textbf{Engagement.}}
	
For the ENG property, consider, for the sake of simplicity, that community C1 displays a higher degree of member engagement than community C2 if the number of project subscriptions (i.e., item 5 from the enumeration in 3.2.4, but any other item may have been used just as well) made by the members of C1 is higher than the number of project subscriptions within community C2. Considering the function number of project subscriptions (PS), we can say that C1 has a higher degree of member engagement if PS(C1) $>$ PS(C2).

To evaluate the representation condition for engagement, it is sufficient to prove that the mechanism which \yoshi uses to determine PS is \emph{consistent}, meaning that it determines a higher PS if the actual PS is effectively higher. \yoshi determines the list of projects-watch subscriptions using the specific GitHub API made for it and uses it to compute the number of project subscriptions. As an example, consider Arduino and Hammer-js projects. 

Inspecting visually the project-watch	number for both, the numbers for the Hammer-js project is 5438 while the number for Arduino is 2071. An analysis provided by \yoshi uses the same numbers and therefore would yield the same result. Note that the remaining points 1 to 7 in the enumeration at Sec. 3.2.4 are added in means, which mathematically does not alter in any way the representation condition for the metric in question.
	
	\paragraph{\textbf{Formality.}}

Formality is determined as the mean membership type in GitHub (+1 for contributors and +2 for collaborators) divided by the milestones per project-lifetime ratio. More formally, $\frac{\bar{a}}{M_{L}}$, where $\bar{a}$ is the mean membership type in GitHub and $M_{L}$ is the milestones per project-lifetime ratio (i.e., the amounts of dates of the total project lifetime which are considered as milestone delivery dates or releases). This quantity increases at the increase of both metric quantities involved (which are both always positive and always different than 0, or the project would have no reason to exist). For the sake of simplicity we focus on showing that, by increasing $\bar{a}$ for a certain project C1 which is higher than another project C2, the quantity reflected by the equation above is proportionally higher. For example, consider that interactions between the members of community Android are more formal than the interactions between the members of community Cloud9 (we know this because of the strict collaboration and participation guidelines behind Android); this means that their relative value $\bar{a}$ must reflect as follows: C1 $>$ C2. According to \yoshi measurements, Android community members collaborate at 0.65 formality and the equivalent value for the Cloud9 project members is 0.43.
%\ANDY{What exactly are you saying with the previous sentence? what does it mean to be ``actively involved''? With the example in the sentence hereafter, it is clear, but the previous sentence alone is not clear enough if you ask me}
Applying a similar analysis to the rest of our dataset, we observed that more formal projects include core developers (i.e., collaborators) in projects that are hosted by the organization in which they are actively involved, meaning that they dedicate to that project no less than 20\% of their working-hours. For example, core developers that contribute to the Pdf-js project (and that are in fact Mozilla employees) --- this further reinforces the more formal nature of such communities.
	
	\paragraph{Summary.}
	Stemming from the above demonstrations, we can answer our research question in a positive manner since \emph{the community characteristic measurement coded in \yoshi are correctly measured by the proposed approach.}\smallskip

	\begin{center}	
		\begin{examplebox}
			\textbf{Summary of RQ$_1$.} The representation condition is valid for all the metrics computed by \textsc{Yoshi}. Therefore, the proposed approach correctly measures the different organisational aspects characterising a software community.
		\end{examplebox}	 
	\end{center}

\begin{table}[!t]
	\centering
	\caption{Modularity Coefficients achieved by the Experimented Approaches - indexes refer to April 2017.}
	\label{tab:rq2}
	%\rowcolors{2}{gray!25}{white}
	\begin{tabular}{lccc}\toprule
		Project & \textsc{Actual Community Structure} & \textsc{Yoshi} & \textsc{Null Model}\\\midrule
		Android & LOW & 0.27 (low) & 0.44 (high) \\
		Arduino & LOW & 0.25 (low) & 0.33 (medium) \\
		BoilerPlate & MEDIUM & 0.31 (medium) & 0.13 (low) \\
		Bootstrap & LOW & 0.23 (low) & 0.28 (low) \\
		Boto & LOW & 0.28 (low) & 0.29 (low) \\
		Bundler & MEDIUM & 0.34 (medium) & 0.31 (medium) \\
		Cloud9 & MEDIUM & 0.35 (medium) & 0.23 (low) \\
		Composer & HIGH & 0.42 (high) & 0.25 (low) \\
		Cucumber & MEDIUM & 0.37 (medium) & 0.33 (medium) \\
		Ember-JS & MEDIUM & 0.38 (medium) & 0.15 (low) \\
		Gollum & HIGH & 0.44 (high) & 0.21 (low) \\
		Hammer & MEDIUM & 0.37 (medium) & 0.26 (low) \\
		Heroku & HIGH & 0.49 (high) & 0.37 (medium) \\
		Modernizr & HIGH & 0.42 (high) & 0.19 (low) \\
		Mongoid & MEDIUM & 0.33 (medium) & 0.24 (low) \\
		Monodroid & HIGH & 0.45 (high) & 0.22 (low) \\
		Netty & LOW & 0.24 (low) & 0.46 (high) \\
		PDF-JS & MEDIUM & 0.31 (medium) & 0.33 (medium) \\
		RefineryCMS & MEDIUM & 0.39 (medium) & 0.19 (low) \\
		Salt & HIGH & 0.43 (high) & 0.21 (low) \\
		Scikit-Learn & HIGH & 0.44 (high) & 0.18 (low) \\
		Scrapy & HIGH & 0.45 (high) & 0.15 (low) \\
		SimpleCV & MEDIUM & 0.36 (medium) & 0.31 (medium) \\
		SocketRocket & HIGH & 0.48 (high) & 0.21 (low) \\
		Hawkthorne & HIGH & 0.42 (high) & 0.25 (low) \\
		\bottomrule
		
	\end{tabular}
\end{table}	

\subsubsection{Does \yoshi provide a meaningful view of the community structure of a software system?}
	
	Table \ref{tab:rq2} reports for each project (i) the actual value for its community structure, and (ii) the modularity coefficients achieved by \yoshi and by the randomly created null models over the 25 considered systems. 
	As it is possible to see, in 100\% the cases the nominal value associated to the coefficients computed by our approach are in accord to the actual community structure: this means that \yoshi provided correct indications over all the 25 subject systems.
	On the other hand, the baseline adequately estimated the modularity of the structure only in six cases, thus being often not able to provide meaningful results. As a consequence, we can claim that our approach not only can potentially accurately assists in understanding the structure of a community, but it is also able to perform better than the baseline. This result is also supported by the statistical tests. Indeed, when comparing the two distributions using the Mann-Whitney paired test, we found that the differences are statistically significant ($\rho<0.001$); moreover, the magnitude of such differences is large ($\delta$=0.74), according to the results achieved when running the Cliff's $\delta$ effect size test~\citep{romano2006delta}.
	
	Interesting is the case of the Scrapy community, which has a high modularity as indicated by the OpenHub data. \yoshi is able to provide a correct indication, since the coefficient computed is equal to 0.45, while the baseline estimates the modularity of the community structure as low. Looking more in depth into the characteristics of this project, we found that this is the one in our dataset having the higher level of interaction among the members, and this is clearly visible looking at the number of pull requests of the project (1,472, of which 154 are still open), and the number of average comments per pull request (5.4). In this case, the detection pattern used by \yoshi is quite effective in the identification of the structure, since it mainly relies on the information captured by pull requests. Conversely, the randomly created baseline wrongly approximated the relationship between the members of the community, thus providing an unreliable result. 
	
	The lower modularity coefficient (0.23) was assigned by \yoshi to the Bootstrap community. Manually investigating this case, we found evidence of the low structure behind this project. For instance, of the 389 contributors involved in it only a small subset of them heavily participate in the activities of the community. Indeed, there are only five \emph{core} committers. Moreover, in most of the cases (i) pull requests are reviewed and commented by these five developers and (ii) discussions about bugs and improvements on the issue tracker only involve them. As a result, our approach correctly marked this community as having a low structure, being able to provide an accurate indication.
	
	The results for the other projects are consistent with the mentioned cases. As a consequence, we can claim that the measures applied by our approach can be potentially effective when employed to understand the underlying structure of the community. As explained before, to further verify this claim we directly involved the most active members of the considered communities, asking them to verify the information provided by our approach. Overall, we obtained 36 answers (1.44 answers per project) out of the 95 invitations sent: therefore, the response rate was 38\%, that is almost twice than what has been achieved by previous papers (e.g., \citep{palomba2015mining,palomba2017scent,VasilescuPRBSDF15}). The response rate was likely pretty satisfactory because of the methodology used to contact developers (direct e-mails): indeed, as done in previous work \citep{silva2016we}, this strategy is generally a good one to obtain quick and effective answers from developers.
		
	Looking at the actual results, we found that 92\% of the times there was a correspondence between the \yoshi output and what reported by developers, meaning that 33 community members fully agreed with the output community structure given by our approach. We believe that this result further reinforces the quantitative findings: indeed, not only is \yoshi able to mine developers' communication and coordination information to discover the corresponding community structure, but it also provides data that reflects the developers' perception of the community. An interesting example regards the RefineryCMS project, where a developer reported:
	
	\begin{quote}
		\emph{``We were dislocated across several countries and for a long while we had communication issues because of that. As a solution, we then decided to start being more selective when accepting members in the community and all the communications passed through our mailing list. This had some benefits, and indeed from that moment we were able to do things in a more cohesive and natural manner.''}
	\end{quote}
	
	This answer clearly refers to the definitions of Formal Networks and Workgroups, as it indicates the presence of both member selection strategies and formal communications---which are typical of FN---but also some degree of cohesiveness between team members, that highlights the presence of a workgroup. In the cases where the developers' answers were not in line with the output of \textsc{Yoshi}, we discovered that it was due to false positive cases in which a Formal Network was interpreted as a Formal Group. Nevertheless, also in those cases we can argue that our approach successfully identified the formality of the communities, thus potentially providing good hints.
		
		\smallskip
	
	\begin{center}	
	\begin{examplebox}
		\textbf{Summary of RQ$_2$.} In 100\% of the cases \yoshi correctly estimated the modularity of the community structures analysed. Moreover, \yoshi's results are much more accurate than the experimented baseline and, more importantly, 92\% of the developers confirmed the correctness of the output presented. Thus, we conclude that our approach can factually support the activities of project managers in the understanding of the community structure behind a software project.
	\end{examplebox}	 
\end{center}

%\begin{table}[!t]
%	\centering
%	\caption{Association Rules between community types and smells.}
%	\label{tab:rq3}
%	%\rowcolors{2}{gray!25}{white}
%	\begin{tabular}{lcccc}\toprule
%		Rule & Support & Confidence & Lift & p-value \\\toprule
%		Formal Group $\rightarrow$ Bottleneck & 0.77 & 0.91 & 1.54 & $0.033$\\ 
%		Formal Group $\rightarrow$ Lone Wolf & 0.73 & 0.88 & 1.26 & $0.013$ \\\hline
%		Informal Community $\rightarrow$ Organisational Silo Effect & 0.74 & 0.94 & 1.69 & $0.011$\\
%		Informal Community $\rightarrow$ Lone Wolf & 0.68 & 0.82 & 1.63 & $0.022$\\\hline
%		Informal Network $\rightarrow$ Organisational Silo Effect & 0.71 & 0.85 & 1.46 & $0.024$\\
%		Informal Network $\rightarrow$ Black Cloud & 0.69 & 0.83 & 1.55 & $0.043$ \\\hline
%		Network of Practice $\rightarrow$ Bottleneck & 0.76 & 0.89 & 1.59 & $0.032$\\\bottomrule
%	\end{tabular}
%\end{table}	
%
%\subsubsection{Is \yoshi usable to highlight the presence of community smells within the project?}
%
%	Table \ref{tab:rq3} reports the association rules, grouped by community type, extracted after the application of the \textsc{aPriori} algorithm \cite{Agrawal:1993}. In the first place, an important consideration is related to the fact that we found relationships between smells and all the communities typified by \textsc{Yoshi}, with the exception of Formal Networks. Likely, this is due to the rigorousness used to select members in this community type: indeed, only certified and acknowledged developers can become members of these types of communities. As a consequence, developers within the community need to follow strict code of conducts to continue contributing. This is the case observed in the release $1.6.0$ of \textsc{Arduino}, where \yoshi identified a formal network whose developers adopted a code of conduct\footnote{http://forum.arduino.cc/index.php?topic=148996.0} to avoid unfriendly behavior among members. This result seems to confirm previous findings indicating that the usage of code of conducts actually supports the activity of software communities by creating a friendly and inclusive environment \cite{Tourani:saner2017}. 
%	
%	On the other hand, other community types are quite prone to be smelly. In our dataset, Formal Groups are strictly connected with two types of community smells, i.e., Bottleneck and Lone Wolf. To discover the reasons behind the relationship, we manually analysed the sources of communications the systems rely on, i.e., the \textsc{GitHub} issue trackers and the mailing lists. Basically, formal groups are formed by people which are explicitly grouped to reach single specific missions. The problems arise in case two groups of the network need to communicate: in these cases it is usual that such groups communicate by means of a representative member, thus naturally leading to the introduction of a Bottleneck smell instance, which appears when there is an overhead due to members interposing themselves into every formal interaction between two groups. An interesting example appeared in the \textsc{Arduino} community, where the communications related to programming questions\footnote{http://forum.arduino.cc/index.php?board=4.0} are often conducted by two specific members, i.e., \texttt{Member A}\footnote{Names of developers are anonymised to preserve their privacy} (present in 2,675 forum posts over the total 3,410) and \texttt{Member B} (present in 2,155 forum posts over the total 3,410). At the same time, the peculiarities of the community type makes it more prone to be affected by the Lone Wolf smell, which represents an extreme case of formal group, i.e., when a small set of developers perform their tasks without caring the decisions made within the group. 
%	Besides having a high support and confidence values, the rules found also have a lift higher than 1, confirming that the two community smells often appear within formal groups. Note that the high lift values are also statistically significant as the Fisher's exact test quantified the $p-values$ as lower than 0.05.
%	
%	The relationships between the Informal Communities and the Organisational Silo Effect and Lone Wolf smells were also quite expected. In this case, an informal community refers to highly-dispersed organisations having a common goal. The high dispersion of developers makes the community intrinsically more prone to be affected by the Organisational Silo Effect, since it may happen that some of the developers do not communicate with others causing poor social connections among the community members. 
%	The high dispersion characteristic also explains the relationship with the Lone Wolf smell: as previously explained, this smell appears when a single developer or a small group of community members start working in isolation without considering the decisions made within the community. Of course, a dispersed environment without a well defined structure is more prone to such behavior because developers cannot physically meet each other daily. The results were also confirmed when looking at the lift value which was higher than 1, being statistically significant (p-values lower than 0.05). 
%	
%	As for Informal Networks, it is worth remarking that this community type does not use governance practices. As such, it is naturally prone to the appearance of a Black-cloud instance, i.e., information overload caused by the lack of structured communication. At the same time, lack of governance also tends to make developers more independent from the community, possibly leading to the introduction of a Lone Wolf smell instance. Also in these cases, the lift values were higher than one while the p-values lower than 0.05: for this reason, we can conclude that the relationships discovered are meaningful and statistically significant. 
%	
%	Finally, we found an unexpected connection between Networks of Practice and the Bottleneck smell. By definition, a network of practice is a community that connects communities of practice, i.e., collocated groups in which interactions are frequent and collaborative. While such communities should theoretically be effective in communication, they are often affected by a Bottleneck due to developers who act as middleman between two groups. For example, in the version  $v2015.5.0$ of the \textsc{Salt} project a single developer managed most of the communications performed by developers, becoming in practice a bottleneck. Interestingly, the lift value reached 1.59 with p-value = 0.032, confirming that the relationship is strong and statistically significant.
%	
%	To broaden the scope of the discussion, the results achieved show that different community types are more prone to be affected by different community smells: this practically means that the information extracted by \yoshi about the community type can be exploited by practitioners as a useful source to diagnose and understand underlying social, socio-technical as well as organisational issues across their community.\smallskip
%	
%	\begin{center}	
%		\begin{examplebox}
%			
%			\textbf{Summary of RQ3.} Different community types relate to different community smells. The reasons behind the presence of smells are strongly related to the characteristics of the community types. As a consequence, we conclude that practitioners can exploit the information provided by \yoshi to diagnose and prevent possible social issues within the community.
%			
%		\end{examplebox}	 
%	\end{center}