%!TEX root = responseletter.tex

\textbf{Answers to Reviewer \#1}

\emph{The paper describes a tool, called YOSHI, that is designed to automatically identify community characteristics in open-source projects. These characteristics are further used to identify community structures. The tool also provides reports and visualizations for better understanding of the results. The authors evaluated their tool on 25 open source projects and identified various patterns and discussed how these patterns can help community shepherds to take decisions with respect to their own or their community's organizational reference.}

\textbf{We would like to thank the Reviewer for the provided feedback that were instrumental in improving our work. We tried to address all comments as detailed below.}
	
\blue{\textbf{To facilitate the new round of review, we have highlighted in blue in the manuscript the main changes/additions (i.e., we did not highlight minor fixes and writing issues).}}

\emph{Evaluation:\\
As authors mentioned, open source projects play a major role in software engineering. Therefore, it is certainly essential to understand the organisational and social structure types, which can help further investigation of community health. Also, the core motivation of the paper that there exist no automated support to identify these structural types is nice. The current paper provides various interesting insights towards this direction. Also, authors made their tool publicly available as well.}

\emph{Overall, the paper is written well, however, many places it gets repetitive and also some of the concepts need examples to make it more clear to the reader.}

\textbf{Thanks, we did our best to improve the first version of the manuscript according to the Reviewer's comments.}

\emph{Authors have certainly conducted extensive evaluation with 25 open source projects, however, the paper is still lacking an important aspect in the evaluation: confirmation from the owners of the subject applications. Currently, all results are based on author's interpretation of the results. It would be great if authors can approach the actual communities and share their findings to receive their feedback.}

\textbf{We honestly believe this is really a great point for improvement, thank you! During the major revision, we tried to address the Reviewer's comment as suggested. In particular, we contacted the developers of the 25 open source communities considered: we limited our analyses to those developers having the highest number of commits (i.e., the most active ones), as they might have a more comprehensive knowledge of the development activities within the community and, therefore, a better overview of the underlying community structure. For this reason, we contacted via e-mail the developers having a number of commits higher than the third quartile of all commits performed on the systems they contributed to, i.e., those contributing the most to each repository, and we asked them to comment about the community structure that was in place in the specific time period analyzed in our paper: to ease the task, we provided them with a spreadsheet containing three tabs: (i) the first reporting detailed information on the commits performed on the repository; (ii) the second with the developers taking part in the development process in the considered time window, and (iii) the list of all communications between developers in the time period. In this way, the developers could better remember the project status in the period, and provide us with more careful observations of the community structure taking place in that period. To further make easy the task, we allowed developers to give us open answers, i.e., we did not provide them with fixed check-boxes reporting the possible community structures. We decided to go for this solution as developers are not likely to be aware of the formal definition of the underlying community structure of their project.}
	
\textbf{Overall, we obtained 36 answers (1.44 answers per project) out of the 95 invitations sent: therefore, the response rate was 38\%, that is almost twice than what has been achieved by previous papers (e.g., (Palomba et al (2015, 2017), Vasilescu et al (2015)). The response rate was likely pretty satisfactory because of the methodology used to contact developers (direct e-mails): indeed, as done in previous work (Silva et al (2016)), this strategy is generally a good one to obtain quick and effective answers from developers.}
	
\textbf{When analyzing the developers' answers, we proceeded with a manual match of their opinions to the automatic community structure assigned by \textsc{Yoshi} to the community a certain developer corresponded to. To avoid any kind of confirmation bias, we recruited two independent external developers having more than 5 years of programming experience (from now on, we refer to them as the inspectors) and asked them to independently perform such a mapping. Specifically, we provided the two inspectors with the developers’ answers and a list composed of the community types extractable using \textsc{Yoshi}. The task was to analyze each of the developers' answers and tag it with one or more community types. For instance, if a developer replied by saying that ``in the considered time period, all developers' communications passed from the mediation of one member, who had the role of disseminating it to other developers'', the inspector could decide to map this answer to the definition of formal community. This process required approximately 1.5 hours. At the end, we first computed the inter-rater agreement between the two inspectors using the Krippendorff's alpha Kr$_α$ (Krippendorff (2004)). Agreement measures to 0.90, considerably higher than the 0.80 standard reference score for Kr$_α$ (Antoine et al (2014)). In cases of disagreement, the inspectors opened a discussion in order to find a joint solution.
In the second place, we verified how many times \textsc{Yoshi} was able to properly identify the community structure perceived by the developers of the considered project. All in all, we found that in 92\% of the times, there were a correspondence between the \textsc{Yoshi} output and what reported by developers, meaning that in 33 cases our approach was able to correctly report the perceived structure of a community. We believe that this result further reinforces the quantitative findings: indeed, not only \textsc{Yoshi} is able to mine developers' communication and coordination information to discover the corresponding community structure, but also provides data that reflects the developers' perception of the community.}
	
\textbf{In the revised manuscript, we included the discussion around the point raised by the Reviewer in Section 4. Once again, we would really like to thank the Reviewer for this extremely insightful comment that allowed us to significantly improve the findings reported in the paper!}

\emph{It seems that the paper includes multiple aspects making it very long, and may confuse the reader. Especially, the whole aspect of community smells and its association with the current work is not clear. Authors can consider removing the community smells and consider it as a different submission. That can help make the current submission more coherent.	}

\textbf{Many thanks for this suggestion. We agree with the Reviewer, perhaps the relation between community types and smells would be better off as part of another submission. This is, indeed, one of the parts making the paper sometimes too long: we accepted this feedback and thus removed this part from the new submission.}
	
\textbf{In addition, following the recommendations of the Reviewer 2, we also phrased the original RQ4 as a discussion section: as correctly pointed out by the Reviewer, the findings we reported might have influenced by other factors that we did not consider. As a consequence, we could not properly answer that research question. For this reason, in the revised manuscript we decided to keep that part, but explicitly presenting it as a use case scenario where our approach might be useful. At the same time, we acknowledge that other factors could influence the observations done.}

\emph{One of the difficult parts of the paper is to understand Algorithm 1. Currently, it is very abstract and difficult to understand how this algorithm actually identify the community types from six characteristics. The follow up sections are very descriptive and clear on how authors measure each of the characteristic. However, it is not clear how these characteristics are being mapped to derive the community type. Request authors to explain the algorithm using a working example. Also, there exist no easy correlation between the characteristics and the ones presented in Table 1.
}

\textbf{Thanks for this comment. In the revised manuscript, we included a working example for the community structure identification algorithm (see Section 3.1, added paragraphs 3 and 4 and added figures 2 and 3). 
}

\emph{In Page 21, authors used three factors as contributions for each community members. How about the members who actually comments on pull requests or signs-off those pull requests? They should also be considered as contributions.}

\textbf{In principle, we agree with the Reviewer. However, since we are interested in restricting the tool's level of analysis to typing the formality of the collaboration structure that is, the level of control exerted across the community (see definition in Sec. 3.2.5 lines 1-3), we chose to focus on operations to contributions that actively change code and hence contribute to making evident the formality of the collaboration structure. This design decision stems from previous experimentation where we tried to factor in also developer comments (which in fact reflect the communication structures across developers, as opposed to their collaboration structure, according to [1]) but ultimately failed because we lack currently an understanding of what makes a comment ``formal'' or ``informal'' in software engineering terms. Nevertheless, we recognise the Reviewer and her valuable point on this matter, and therefore discuss this limitation in the threats to validity, pointing out to future work in this direction (see Sec. 6.3, Par 1, lines 11 and following).}
	
\textbf{\noindent [1] Fuks, Hugo, Raposo, Alberto Barbosa, Gerosa, Marco Aurelio and de Lucena, Carlos Jose Pereira. ``Applying the 3C model to groupware development.'', Int. J. Cooperative Inf. Syst. 14 , no. 2-3 (2005): 299-328.}

\emph{In Page 21, in the case of Community cohesion, what is the rationale behind the restriction that the members should share significant expertise overlap? Please explain.}

\textbf{We inherit the term, its definition, and rationale from the state of the art in working-groups [1,2,3]; the definition in question, includes a strong connotation of community cohesion [1,2] which is associated to low cognitive distance among members and hence, high expertise overlap [4]. We cited all the papers below in the new version of the manuscript when referring to the operationalisation of community cohesion.}

\textbf{\noindent [1] Moody, James and White, Douglas R. ``Structural cohesion and embeddedness: A hierarchical concept of social groups.'' American Sociological Review (2003): 103--127.}

\textbf{\noindent [2] Hung, Hayley and Gatica-Perez, Daniel. ``Estimating Cohesion in Small Groups Using Audio-Visual Nonverbal Behavior.'' IEEE Trans. Multimedia 12 , no. 6 (2010): 563-575.}

\textbf{\noindent [3] Giraldo, Luis Felipe and Passino, Kevin M. ``Dynamic Task Performance, Cohesion, and Communications in Human Groups.'' IEEE Trans. Cybernetics 46 , no. 10 (2016): 2207-2219.}

\textbf{\noindent [4] Nooteboom, Bart and Vanhaverbeke, Wim and Duysters, Geert and Gilsing, Victor A. and van den Oord, Ad, ``Optimal Cognitive Distance and Absorptive Capacity'' Springer (April 2006).}

\emph{In Page 28, what do the number of stars mean? Can you provide more details?}

\textbf{The star rating in question refers to the amount of stars that each project was granted during its story - the number of stars can be seen by navigating to the page of the project (e.g., the GitHub repository for Apache Spark: https://github.com/apache/spark) and reporting the number of stars captured in the top right of the splash-page for the project (e.g, 17,791 stars in the case of Spark).}

\emph{In Page 28, Table 3, these are the main results of the paper, since the intention is to discover the community types. However, authors did not present any insights on the discovered types. Also, some of the community types such as project teams are not discovered in any of the subject applications. Is there any specific reason? Please explain.}

\textbf{The Reviewer is very right in this comment - in trying to make the paper as clear as possible while also trying to capture the most critical details for YOSHI and its evaluation we actually diminished the content discussing further the types that were found and how they reflect our sample. Therefore, we augmented the discussion of types found in Section 4.2, trying to limit our interpretation and keep the account minimalistic, reporting the available evidence and focusing on its summarisation. The same augmentation is also recalled in Section 5 to conclude that the tool is in fact a valuable asset for open-source community health-tracking.}
	
\textbf{In addition, as explained above, we also conducted an additional analysis aimed at understanding the value of YOSHI from the developers’ perspective (see Section 4.2.2).}

\emph{For RQ4, authors considered four repository related metrics: (1) mean number of commits per month, (2) mean number of contributors per month, (3) number of stars, (4) number and forks repository has on Github. What is the rationale behind these metrics?}

\textbf{The metrics in question constitute a generally-acknowledged set of metrics for the quality of open-source projects and surrounding communities, we inherited these values from the state of the art, as specified in the new discussion section (Section 5). More specifically, as mentioned in the referred section, the metrics were selected from [1] and [2].}
	
\textbf{[1] [Jansen(2014a)] S. Jansen. Measuring the health of open source software ecosystems: Beyond the scope of project health. Information \& Software Technology, 56(11):1508–1519.}

\textbf{[2] [Crowston et al.(2012)] K. Crowston, K. Wei, J. Howison, and A. Wiggins. Free/libre open-source software development: What we know and what we do not know. ACM Computing Surveys (CSUR), 44(2):7, 2012.}

\emph{Also, in RQ4, authors mentioned that communities having a strong level of formality have lower turnover in terms of constant number of contributors. It is not clear how this conclusion has been derived. For example, the number of contributors is high in the case of \{NOP, IN, FG\}. Request authors to explain in detail.}

\textbf{The Reviewer is referring to section 4.2.4, paragraph 3, lines 12 and following, which read: ``Nevertheless, such a change within a community is something that a community shepherd might desire or not, even because a different community design patterns might be associated with some negative manifestations or lead to different characteristics: for instance, as reported in Figure 6 a change towards an informal community might decrease the overall contributors continuity and increase the number of forks of the project. To account and manage such compromises, a tool like the one proposed in this paper might be nicely adopted by community shepherds during the decision making process.''}

\textbf{Within the phrasing in question, the reviewer will notice that we make no conclusion directly here, we simply state that one pattern might lead to a specific lower (or higher) manifestation of a certain characteristic - our conclusion is that we found important differences among the community patterns inferred by \textsc{Yoshi} with respect to four health metrics considered and that the tool should be used and experimented with further, especially to understand the perplexities and possible research questions, including the one originally intended by the reviewer (see Section 5 of the revised manuscript).}

\emph{Page 38, in Tool limitations, authors mentioned that YOSHI is limited to establishing the presence of 8 out of 13. However, YOSHI actually can find 9. Please fix.}

\textbf{Thanks, it was a typo. We fixed it in the revised manuscript.}

\emph{Minor issues:\\
Page 4, Thes -> These\\
Page 9, It is better to define acronyms before they are being used. For example WG was used first before defining it as WorkGroup.\\
artefact -> artifact\\
Page 18, ``... specified location form their own profile..'' -> ``... specified location from their own profile..''}

\textbf{Fixed.} 



