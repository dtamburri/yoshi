
	This paper introduces \textsc{Yoshi}, a tool for the automated organisational structure pattern detection across open-source communities. \yoshi gathers information about open-source communities and projects and executes necessary preprocessing operations to obtain information about community characteristics. Finally, \yoshi uses community characteristics found to infer a pattern of open-source community structures. \yoshi stores and reports results and offers a modular design, ready for further extension and analysis. The rest of this section recaps conclusions and future work beyond the scope of this article.

\subsection{Lessons Learned}

	While testing \yoshi on 25 projects from GitHub, we observed that:
	
	\begin{enumerate}
		\item \textbf{\yoshi allows the reliable prediction of community structure patterns using software engineering data.} The experiment conducted to verify the accuracy of the tool revealed that it offers a valid set of metrics which satisfy the basic representation condition as defined in literature \citep{repcond}. Furthermore, \yoshi provides an accurate information with respect to the community structure according to our null-model analysis.
		
		\smallskip
		\item \textbf{\yoshi can be used to monitor and manage social debt within open-source communities.} The design pattern output by the proposed tool can be used by practitioners to control the quality status of social and organisational relationships among developers of a community: indeed, each community type has its own peculiarities and might reflect the presence of specific social debt items \citep{jisaspecissue,cacmqualcomm} within the community.
		
		\smallskip
		\item \textbf{\yoshi can be used to steer repository popularity by comparison.} Our analyses revealed that informal groups and networks tend to have a higher number of stargazers and forkers, thus having a higher likelihood to be reused by other projects. As a consequence, the pattern detection facility provided by the tool can be exploited to monitor the repository status and plan specific preventive actions aimed at increasing its reuse-proneness.
	\end{enumerate}

	From the above features we can conclude that \yoshi: (a) allows reuse of type-specific community steering and adaptation best practices from literature; (b) yields deeper understanding of open-source communities' organisational and socio-technical nature; (c) offers a valuable basis for diagnosing open-source community structures and design patterns thereof.

\subsection{Future Work and Outlook: Forming the Software Community Shepherd}

Our prototyping and experimentation with \yoshi showed us that while sites like Bitergia and OpenHub may provide vital insights into open-source communities they do little to elicit and measure key community design pattern performances. Conversely, the community shepherd is a persona whose goal aims at: (a) measurably understanding the organisational structure requirements behind software, to encourage sustainability; (b) applying models, techniques, and approaches from organisations research to measurably improve the open-source community structure, to encourage continuous improvement; (c) making the organisational structure characteristics more transparent, open, and measurable to encourage further research.

	Further experimentation is needed along the above research path.
	In that respect, \yoshi offers ample opportunity for improvement and further work. For example, \yoshi can be extended to include techniques such as more elaborate data-mining or sentiment analysis~\citep{Novielli:Calefato:Lanubile}. This refines \yoshi even further  in supporting governance and management across open-source communities.
	
	In addition, \yoshi may be combined with tools that detect the technical qualities of open-source communities with the aim of eliciting and evaluating a full-fledged community quality model complementing well-known software product quality models~\citep{FerencHG14}---we started researching along this path \citep{cacmqualcomm} but we concluded that we barely scratched the surface of a vast array of possibilities that require further research.
	
	Moreover, further experimentation is needed to establish which patterns elicited by \yoshi actually correspond to which community smells and in which technical conditions. This analysis may reveal open-source organisational patterns which are best fitting with specific domains, or products.
 
	Finally, \yoshi should be extended to cope with closed-source organizations as well. This extension entails an additional set of metrics to be devised to integrate remaining community patterns from \citet{specissue,ossslr}. Also, this extension calls for an additional round of validation in closed-source software projects. In the future we plan to address the tool's technical limitations, while providing more ample empirical evaluation, possibly over multiple software forges other than GitHub.