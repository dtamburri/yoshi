%!TEX root = yoshi.tex
There are several works related to \textsc{Yoshi}, mainly residing in the general areas of open-source community governance studies and computer-aided open-source management. In this section, we overview relevant previous papers in these fields.

\subsection{Governance and Community Aspects in Open-Source}

First and foremost, works that strive to understanding and steering the specific coordination and governance models adopted in an open-source project are highly related to the proposed one. This is critical both for developers to assess whether to contribute or not to the project and  for final users of the resulting application, since trust in the community can vary strongly according to the governance  mechanism underlying the development.
\citet{RePEc:wpa:wuwpio:0312005} identified three
main categories of projects:\footnote{A similar categorisation has been proposed by \citet{Grex:generations}.}
\begin{itemize}
\item \emph{\bf Corporate projects}, entirely developed within a single company and
then released as open-source.
\item \emph{\bf Voluntary projects}, which are supported by volunteers only, offering
their efforts and resources without being remunerated.
\item \emph{\bf Hybrid projects}, jointly developed by volunteers and employers working
for the company which runs the project itself.
\end{itemize}
An example of voluntary project is represented by
Debian\footnote{\url{http://www.debian.org}}, a completely free
operating system launched in 1993 by Ian Murdock. 
One of the most relevant characteristics of the organization model
adopted by the Debian community consists in the adoption of the Debian
Social Contract, a document listing the moral rules and the values
at the basis of the project itself. 
The coordination mechanisms in place within the project are defined
within another formal document, the Debian Constitution\footnote{\url{http://www.debian.org/devel/constitution. en.html}}. The governance
structure is hierarchical and includes different roles, such as the
Project Leader, annually elected by developers, the Technical Committee,
mainly responsible for technical issues or problems related to overlapping
responsibilities, and developers, managing the packages they are in
charge of \citep{RePEc:wpa:wuwpio:0312005}.

Code of conducts in open source have been also the object of the study by \citet{Tourani:saner2017}, who investigated role, scope and influence of codes of conduct in practice. 
Key findings of their work report that thousands of projects rely on code of conducts to manage the behavior of project's members and avoid unfriendly environments, and they use to stipulate contracts targeting all collaboration spaces of the community, trying to fix strict rules for collaborators. 

\citet{coelho2017modern} investigated the causes behind the failure of modern software systems. The described a set of nine reasons, and interestingly five of them were related to team- or environment-related issues. For instance, a notable amount of them failed because of conflicts between the contributors. 

With respect to these papers, it is important to remark that \textsc{Yoshi} does not directly identify governance models, specific coordination requirements, or code of conducts, but it captures a snapshot of open-source communities' current organisational and social layout by analysing their key characteristics. Appropriate governance models should be selected after (or evaluated with) the results achieved by \textsc{Yoshi}, and can be adopted to monitor how the overall community is evolving and whether the specific pattern is follows might create conflicts or coordination problems that potentially lead to more serious issues.

Even considering similar communities, it is still possible to identify
differences in the governance practices they
follow. \citet{Prattico} has considered
six communities supported by active open source foundations: Apache,
Eclipse, GNOME, Plone, Python and SPI. Using computer-aided
text analysis of each foundation's bylaws, Prattico noted
that, although each foundation adopted different terms, it was possible
to identify three common main power centers: the \emph{members of the community},
the \emph{board of directors} and the \emph{chairman of the community}. For example,
the chairman of the community can be named by the board of directors,
as in the Eclipse foundation, or elected by the members, as in the
Debian project. 
The board of directors is composed of people elected
by the members. The board takes decisions about the piece of software
it is in charge of. Also, different communities showed a different distribution
of power. 
For example, in the Eclipse Software Foundation power is
mostly managed by the chairman, while in the Apache Software Foundation
the board of directors and the members exert the most power, with
an inclination towards the board of directors.
Given the above works, it becomes critical for software engineering
theory and practice to learn as much as possible from open-source
communities and their ways of coping with GSD (Global Software Development). 

Also in this case, \textsc{Yoshi} does not directly uncover best-practices concerning community aspects in open-source but it does allow further reasoning such that best practices can be developed with additional study.

Similar works have been done by \citet{onoue2014software,onoue2016investigating}, who studied the population structures of open source systems: in particular, they first defined a method for predicting the survival of open source community members within a software project, and then investigated the pyramidal roles present in such projects. With respect to these works, our paper can be seen as complementary, as it proposes an approach to compute community-related metrics and understand what is the underlying structure of community, so that practitioners can take informed decisions on how to evolve it.

\subsection{Computer-Supported Cooperative Work in Open-Source}
Usually, communication within a distributed team, either open-source
or closed-source,
is supported by forges typically including mailing lists and Web-based tools like forums and
wikis. Contributions are shared by means of Concurrent Versions Systems
(CVSs) or Distributed Version Control Systems (DVCSs), like Subversion
or Git, which provide versioning features, allowing to easily check
or revert someone else's contributions. Moreover, tracking systems
are used by the community itself and by external users to report bugs
or other problems and to ask for the development of new features.

Several studies have been dedicated to identifying social and technical barriers for newcomers' participation in
Open Source~\citep{steinmacher2015social,Mendez:et:al,FordSGP16,BalaliSASG18,VasilescuFS15}.
Going beyond identification of the barriers, some tools are explicitly aimed at tackling one or more 
technical barriers often related to communication, e.g., by allowing practitioners to talk 
with remote colleagues in an easy and informal
way. For example, the Jazz project, sponsored by IBM, added instant
messaging features to the Eclipse Platform, together with other tools that show which files are currently being edited by whom
\citep{Cheng:2003:JUE:965660.965670}. Also, a number of tools focus on supporting
activities such as distributed problem analysis, requirements elicitation and
activity planning. For example, the tool MasePlanner is an Eclipse
plug-in with features for simple agile planning in distributed
contexts. Users can create story cards shown in a common virtual
workspace. Cards can be organised and modified by project members
to plan their activities. 

Other tools aim at improving awareness by extracting information from
forges. For example, tools proposed by the Libresoft group
mine data extracted from code repositories, malinglists, discussions
and tracking systems \citep{robles2011:floss_datamining}. The
SeCold portal adopts mining techniques to build a shared knowledge
base about several open-source projects, including explicit facts
like code content and statements, as well as implicit data, such as
the adopted license and the number of clones produced from a project
\citep{MiningKeivanloo}. In similar studies, mining techniques are used
to extract patterns to represent and improve the decision process
adopted in software development.

Finally, the ALERT\footnote{\url{http://www.alert-project.eu}} project uses ontologies
to  increase awareness by gathering and linking related
information obtained from the different tools adopted by the
community, including structured and unstructured sources \citep{stojanovic2011alert}. ALERT is intended to use this information to build personalised, real-time
and context-aware notifications, to detect duplicate bug reports and assigning bugs reports to be solved.

To the best of our knowledge, there is no computer-assisted tool support for identifying and measuring the community, social and organisational aspects behind open-source communities. 
Yet, studying these aspects is essential for capturing organisational and social best-practices from the widely discussed and studied open-source phenomenon.
