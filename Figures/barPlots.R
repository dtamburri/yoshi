

commitsPerMonth <- c(771,709,644,502,305)
contributorsPerMonth <- c(55,51,24,28,34.5)
stars <- c(4779,5899,3453.5,6847,10588)
forks <- c(1976,1639,2165,2847,2199)

names <- c("FG","FN","IC","IN","NOP")

par(mfrow=c(2,2))
  barplot(commitsPerMonth, main="Commits/Months", names.arg=names, ylim=c(0,800))
  barplot(contributorsPerMonth, main="Contributors/Months", names.arg=names, ylim=c(0,60))
  barplot(stars, main="Number of Stars", names.arg=names, ylim=c(0,11000))
  barplot(forks, main="Number of Forks", names.arg=names, ylim=c(0,3000))
  
  commitsPerMonth_group1 <- c(25,11703,2345,116)  
  commitsPerMonth_group2 <- c(1,1278,8,664,30,502,104151) 
  commitsPerMonth_group3 <- c(66, 833, 1560) 
  commitsPerMonth_group4 <- c(709, 1032, 1913) 
  
  contributorsPerMonth_group1 <- c(9,688,173,11)  
  contributorsPerMonth_group2 <- c(1,160,1,85,2,51,1140) 
  contributorsPerMonth_group3 <- c(8,41,146) 
  contributorsPerMonth_group4 <- c(27,334,264) 
  
  stars_group1 <- c(6523,7764,17938,3501)
  stars_group2 <- c(73,9510,8298,10255,234,6847,1988)
  stars_group3 <- c(16397,18211,111537)
  stars_group4 <- c(19148,4779,2554)
  
  forks_group1 <- c(1976,3600,3723,1236)
  forks_group2 <- c(38,4706,1320,4983,138,5506,2847)
  forks_group3 <- c(2307,4409,51503)
  forks_group4 <- c(10416,2091,2307)
  
  par(mfrow=c(1,4))
  
  vioplot(commitsPerMonth_group1, commitsPerMonth_group2, commitsPerMonth_group3, commitsPerMonth_group4, 
          names=c("FN,FG,WG", "IC,FN,FG", "IN,NOP", "NOP,IN,FG"), col="gold", horizontal = TRUE)
  title("Commits/Month")
  
  vioplot(contributorsPerMonth_group1, contributorsPerMonth_group2, contributorsPerMonth_group3, contributorsPerMonth_group4, 
          names=c("FN,FG,WG", "IC,FN,FG", "IN,NOP", "NOP,IN,FG"), col="gold", horizontal = TRUE)
  title("Contributors/Month")
  
  vioplot(stars_group1, stars_group2, stars_group3, stars_group4, 
          names=c("FN,FG,WG", "IC,FN,FG", "IN,NOP", "NOP,IN,FG"), col="gold", horizontal = TRUE)
  title("# Stars")
  
  vioplot(forks_group1, forks_group2, forks_group3, forks_group4, 
          names=c("FN,FG,WG", "IC,FN,FG", "IN,NOP", "NOP,IN,FG"), col="gold", horizontal = TRUE)
  title("# Forks")
  
  