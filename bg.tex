%!TEX root = yoshi.tex
%\subsection{Organisational Structures: Definitions and Quality}\label{struct}

This section outlines the background in organisational structures, providing a general overview and definitions. Subsequently, the section discusses the background and general objectives of organisational structure quality research and how it relates to software engineering in general and our tool in particular. Given that the background section is dense with concepts and definitions not strictly part of software engineering research but interdisciplinary in nature, in the following we offer a nutshell summary---the interested reader can find full details in the remainder of the section. 

A \emph{software development community} is a specific type of social network upon which 
certain properties constantly hold (e.g., informal communication across electronic channels of open-source 
projects) \citep{ossslr,cacmqualcomm} across \emph{community members}, that is, the set of people who 
interact in any way, shape, or form with the \emph{practice} reflected by the community (e.g., a software 
product).

Across such development social networks and their many possible properties (e.g., informality, goals, membership selection, intercommunication protocols, etc.), communities can develop sub-optimal conditions which we previously defined as \emph{community smells} \citep{jisaspecissue,PalombaTSZFO18} in analogy to code smells---the analogy signifies that, on one hand, community smells identify unlikable circumstances (e.g., the lack of communication across different modules of a software system) but, on the other hand, these conditions do not necessarily stop or void the organisational behaviour across the community, rather, they prove detrimental and cause additional project cost in several possible ways (e.g., recurrent delays in communication, wrongful knowledge sharing).

Finally, with the term \emph{project}, we identify the goal or shared practice that the community maintains as its central endeavour, e.g., the Apache Spark community holds the delivery of the Apache Spark product as its key \emph{project}.

\begin{framed}
\textbf{Background and Goals Digest.} A community type is a social network where certain characteristics are constantly true, for example, an informal community is a social network where all interactions are \emph{always} informal. Disciplines such as organisations research and social-networks analysis study community structures and types to measure and manage their salient characteristics to socially healthy and organisationally performant levels. \yoshi is a tool that applies that intelligence and knowledge to detect structural design patterns across open-source software engineering communities, and is able to identify nine types using their unique identifying characteristics. Our ultimate objective is using \yoshi and community patterns as instruments to assess open-source organisational quality.%, for example, to predict that certain community anti-patterns, a.k.a. community smells, are more or less likely.
\end{framed}

\subsection{Organisational Structures Explained}

The literature in organisational structure research resides mostly in the following fields:
\begin{itemize}
\item Organisations research---in this field organisational structure types and characteristics represent more or less effective consequences of \emph{organisational design}, i.e., the management activity of planning a strategic organisational agenda around a pre-specified organisational structure \citep{Chatha2003};
\item Social-Network Analysis---in this field organisational structure types and characteristics represent measurable quantities that can augment social-networks from any context or domain (networks of people, communities of partners, networks of organisations, etc.) \citep{sna,sna2};%\FABIO{Insert citation}
\item Cognitive Ergonomics---in this field organisational structure types represent models that allow reasoning on transactive-memory processes \citep{NevoW05} (i.e., who knows what, where, etc.), information representation, as well as information exchange policies;
\end{itemize}

The following sections offer more precise definitions of organisational structures, their types and characteristics as well as outline their role in the context of this study.

\subsubsection{Organisational Types and Their Characteristics}

Several seminal works address organisational types in the state of the art of software engineering. For example, \citet{Mockus2002} investigate Mozilla and Apache, characterising quantitatively and qualitatively their organisational structure, but without explicitly associating a type (i.e. a set of social and organisational characteristics) from the state of the art. Conversely, for the benefit of software engineering research and practice, in our own previous work \citep{ossslr} we strived to summarise the insights on organisational structures from the fields above as well as others, into common themes or \emph{types} of structures. In layman terms, a structure type is a set of measurable or otherwise evident organisational characteristics (e.g., the presence of informal communication channels across an organisation). Based on how organisational characteristics influence the structure, the way of working in the structure can change radically. For example, the way of working in a Community of Practice (collocated, tightly knit, practice-focused) is different than that of a Formal Network (formal, distributed, protocol-based). Also, if characteristic X has its \emph{highest} manifestation in a certain type, X can be used as an \emph{identifying indicator} for that type, that is, the primary characteristic which is a necessary condition for its identification \citep{ossslr}. For example, Formality is a primary indicator for organisational structures with well-defined rules and regulations, typically dictated by corporate governance. More precisely:

\begin{center}
\emph{Organisational Structure Type:\\}
$\omega = [\delta(C_1) +, ..., + \delta(C_n)]$;
\end{center}

\noindent where $\omega$ represents the organisational structure type as a ``sum'', i.e.,  the combined effect of organisational and social characteristics (C$_1$,..., C$_n$). On the one hand, the characteristics themselves are heterogeneous, for example, some refer to the community's location (e.g., virtual, situated) and some refer to the closeness of community interactions (e.g., cohesion, informality). On the other hand, all these characteristics can be quantified by means of observability functions ($\delta$), i.e., sensing functions which assign a Likert-scale value based on the level of influence that each characteristic bears on the structure according to its members/participants. For example, an Informal Network type is strongly indicative of \emph{informal communications} and might lead to \emph{engaged members} \citep{ossslr}. Only informality is \emph{necessary} for the identification of Informal Networks, and hence, a unique indicator for such types. If indeed in addition to informal communication a high degree of engagement has been observed, then we consider this highly-engaged version of Informal Networks as a distributed version of Informal Community. Fluctuation of engagement levels in this instance, during the evolution of the organisational structure, can reflect changes from Informal Community type to Informal Network or vice versa.

\begin{framed}
\textbf{\yoshi Analysis Lens}. \yoshi focuses on detecting community design patterns using the characteristics and types evident across an observable community, hence determining the \emph{pattern} of types that the community exhibits across its organisational structure.
\end{framed}

% --- CLARIFY THE EXAMPLE WITH SPECIFIC TIME "CONSTRAINTS" AND WITH THE SAME TERMS
%In this case, the two types, both informal, may be alternating themselves according to the phase of the organisational structure; for example, think of an informal, highly-distributed, open-source community which becomes extremely engaged at release-time. 

As an example of the equation above for IC see the following:

\begin{center}
\emph{Organisational Structure Type IC:\\}
$IC = [ \mbox{\em Informality(High) + Engagement(High)} ... ]$;
\end{center}

Figure \ref{fuzzy} visualises the point above, using the example pattern:%\\ 
\begin{center}
$IN,WG = [ \mbox{\em Informality(High) + Cohesion(High)} ]$;\\ 
\end{center}
in the example, a likely scenario reflects a set of globally dispersed software practitioners working over the same open-source product (e.g., a video-game) constitute an Informal Network which can show high cohesion (adding in the primary characteristic of Working Groups) when practitioners meet face-to-face (e.g., at comic conventions, ``Comicons'', or gaming tournaments). \yoshi would identify a single pattern including both types blended together for the ``Comicon" community snapshot. Nevertheless, these two types may diverge into other types later on in the community lifetime, e.g., into a formal type during release. \yoshi currently returns types whose identifiers remain the highest and over a certain threshold for the entire duration of the observed 3-month snapshot. Consequently, considering Fig. \ref{fuzzy} \yoshi would return a pattern constituted by both types only in correspondence of the point in time when both Informality and Cohesion are highest, and Informal Networks otherwise.
%\ALEXANDER{Nice example ahahha thanks!}
%\ALEXANDER{This example seems rather artificial. Can you try to tell a story about a project X that starts with a small group of enthusiastic students, working in an informal way etc.? This still can be an $IN+WG$ combination, but probably the reader will be able to relate better to the story...}
%\ALEXANDER{Wouldn't this be \emph{one} blend?} 

\begin{figure}
	\begin{center}
		\includegraphics[scale=0.45]{Figures/fuzzy}
	\end{center}
	\caption{An overview of the nature of organisational and social characteristics behind communities - our tool predicts community design patterns by evaluating the trend of the perception curves for primary community type indicators. The figure also reports (right-hand side) the ``Comicon" event in correspondence to two identified types, from our example.}\label{fuzzy}
\end{figure}

%From the above definition, it follows that different types may be have a high or low manifestation of the same characteristic at different points in time. Conversely, different characteristics may have high (or highest) values at the same time, since some describe the way in which the community interactions take place, while others describe what those interactions are aimed at. For example, Informality or Engagement are primary structural indicators in structures that rely on informal communication, as we would expect for most open-source communities. As opposed to this conjecture, many open-source communities specify rigorous codes of conduct, the restrictions that behold all members of the community, rather than specifying \emph{how} the community operates. For example, High-cohesion and Duration are primary in small team scenarios, where 3-10 people are bound to a timeframe by contractual arrangements that establish a small and cohesive project team. 

In summary, a single organisation can exhibit the traits of multiple types at once and even very different \emph{or conflicting} types, over time---meaning that multiple, sometimes even conflicting characteristics, often blend into the same organisational structure. Addressing organisational conflict is a key objective of organisations and social-networks research~\citep{jeppesen2011attitudes,Fredrickson:1986:amr}, and, thus, is a fundamental part of our motivation to support automated detection of community design patterns in open-source.

\input{tableOverview}

\subsubsection{A Methodology to Discover Organisational Patterns in Software Engineering}

In the recent past, a number of studies were aimed at understanding community types and their role in software engineering as well as at finding ways to use community types as reference patterns during software processes. Literature review reveals a total of more than 70 organisational and social structure characteristics \citep{ossslr} to be measured for fully describing community structure types. Out of these characteristics a total of 13 characteristics were distilled, each individually reflecting a single type. In the scope of this paper, we focus on detecting community design patterns which feature the six characteristics that we were able to operationalise for open-source communities, namely, (1) community structure\footnote{The first characteristic, structure, is a necessary pre-condition to all of them; in fact, all communities are social-networks (SNs) that exhibit a community structure across which certain characteristics remain constant.}, (2) formality, (3) engagement, (4) cohesion, (5) longevity and (6) geodispersion. These aforementioned characteristics were operationalised in \yoshi as an original contribution of this paper (see Section \ref{sec:approach}).

In fact, contrarily to literature in organisations research \citep{Prandy2000,Mislove2007,Ryynanen12} where organisational types and characteristics are studied \emph{qualitatively}, as an original contribution of this paper, we measure the \emph{quantitative manifestations} of community characteristics, namely, we use quantitative, automatically measurable indicators of the perception functions introduced previously. For example, to measure engagement, we evaluate together the amount, frequency, and kinds of contributions of an open-source community member with respect to its peers. 

In our early exploratory experiments with community types and patterns while designing \yoshi automations, we observed that (1) different levels of the same characteristics correspond to different types and (2) measuring open-source communities reveals at least two co-existing types. From this early experimentation, we made two observations. 
First, \yoshi must be designed to detect \emph{design pattern}s composed of recurrent community characteristics and their corrresponding types. 
Second, it is not sufficient to only measure the six characteristics above. Automated detection of organisational design patterns demands a way to identify the level of their highest manifestations above all remaining characteristics such that the most prominent community types can be revealed and distilled into a pattern. 

Consequently, we engaged in and contributed to a highly active open-source community along a 15-month ethnographical study of its organisational structure \citep{titans}, for the purpose of determining empirical thresholds to all our primary community indicators.

Table \ref{overview} provides an overview of the above results, briefly describing community types, their indicators, as well as highlighting the empirical thresholds elicited as part of our ethnographical research \citep{titans}. The thresholds allow determining high or low values for community indicators, thus allowing identification\footnote{The interested reader can find detailed information and full characterisation of each type in our previous work~\citep{ossslr,archcomm,specissue}}. 

In what remains of this subsection, we provide an overview of the thresholds that we mention in Table~\ref{overview}. In particular, in previous work \citep{titans}, we were interested in ways to measurably increase the awareness of open-source developers over known organisational and socio-technical characteristics of communities from organisations and social-networks research (see Table \ref{overview}). For this reason, one of the co-authors of this study along with two master students started contributing to Apache Allura, an open source community building the infrastructure behind SourceForge, a widely known open-source forge. In this process of contribution, the following data was gathered for the first interaction by the three observers: (a) guidelines of contribution; (b) code of conduct across the community; (c) expected contribution. Moreover, for the rest of our 15-month involvement, every other interaction with the community was documented as follows: (a) type of interaction (direct/indirect); (b) involved actors (presence of communication intermediaries); (c) means of communication (e.g., formal/informal means); (d) perception of ``tone" of communication (formal/informal); (e) location of the involved participants and organisations; (f) explicit/implicit guidelines for contribution in question; (g) previous members' relation with observers or amongst themselves; (h) delay in response. Finally, the following data was elaborated in a conclusive summary of the community: (a) skills profile of community members; (b) roles and responsibilities; (c) organisational structure sociogram \citep{sna}.

Subsequently, we sought to associate a ground-truth set of community types and characteristics corresponding to the data thus obtained. Hence, at the end of the 15-month study, we asked 7 top-level contributors to Allura their perceived values over the characteristics from Table \ref{overview} and their perceived open-source community type(s), if any. Through this process, Allura was determined to be a \emph{Formal Network} type blended with a \emph{Network of Practice}---this  empirically defines two thresholds for the two primary characteristics that manifest for those types: (1) \emph{Formality} - the highest primary characteristics reflecting formality in Allura would define our Formality threshold; (2) \emph{Geodispersion} - the average geographical and cultural distance between Allura members would define our Geodispersion threshold. 

Concerning the remaining characteristics, we analysed our data on developer interactions. First, we observed \emph{Informality} manifesting itself among the four core Allura maintainers. Focusing on the interactions among the four developers in question, we isolated their commonalities (e.g., they all shared previous relations on other projects, they all shared at least three background skills, etc.) and evaluated thresholds for resulting factors.
% For example, previous informal relations outside of Allura were found to be a primary factor for informal interaction. 
Similarly, we observed that \emph{Engagement} and \emph{Cohesion} of Allura developers were very high when the community was closing in on a release of its platform. Consequently, we measured Cohesion (represented by the well known social-network analysis metric \citep{sna}) and Engagement levels (represented by summing all possible contributions that members would make to the release of Allura and computing an average).
%Finally, for \emph{Situatedness} we used same threshold its evaluation is by definition the opposite of \emph{Geodispersion}; consequently, the applies.

In the same study, to strengthen the validity of our thresholds, we measured and empirically evaluated the metrics and thresholds for an additional four communities hosted on SourceForge, seeking and successfully evaluating the agreement of those communities' members with our type predictions.

In the scope of this article, we sought to operationalise the metrics defined and evaluated in our previous work \citep{titans} offering three tool-specific contributions beyond previous work:
\begin{enumerate}
\item a tool designed for \textbf{large-scale use}: in our previous study the measurements and empirical analysis was conducted by hand, using crude statistical analysis and focused on distilling the type of four communities only, while in this article we focus on offering an automated tool designed for large scale use and using GitHub data. Moreover, the empirical evaluation in the scope of this article encompasses 25 randomly-sampled open-source communities.
\item a tool designed for \textbf{precision}: in order to be actionable, a type prediction needs to be accurate; in our previous study we used a single quantitative metric per every primary characteristic, while with \yoshi we provide between 1 and 3 non-overlapping metrics in the detection pattern of characteristics for which our prediction in previous work was imprecise. Moreover, we offer an evaluation of \yoshi precision using null-model analysis.
\item a tool intended for \textbf{further replication} and open-source release; our study of community design patterns in open-source reflects the fundamental research of open-source organisational structures and we want to encourage others to pursue the research path we are currently exploring. In this study we offer a completely free and open-source replication package to call for, and encourage verifiability.
\end{enumerate}

As a result, the study reported in this article offers a more precise, scalable, replicable, and verifiable tool along with its empirical evaluation results.

\subsection{Organisational Structure Quality}
Despite the fact that previous work on open- and closed-source software communities does in fact offer relevant insights into the characteristics of the different organisational structure types, it is important to note that: (i) there is still a lack of tools that provide automatic identification of community characteristics and type; (ii) previous work has been mainly oriented toward industrial environments, thus missing a detailed analysis in the context of open-source teams, which are becoming ever more important for the development of both academic and industrial software \citep{impact,Crowston2012}. 

Such an analysis is of paramount importance to highlight commonalities and differences among the different organisational structures in different development contexts, and to understand to what extent the management and evolution of open-source systems may benefit from the usage of community-related information. Moreover, some organisational types may work better than others for the purpose of software engineering and evolution; this line of inquiry reflects organisational structure quality and can be assisted by the use of tools such as \yoshi which map open-source communities onto known organisational types and characteristics  and their \emph{quality}. \medskip

The quality of an organisational structure generally refers to the organisational structure's fitness for purpose, i.e., the measurable degree to which the structure is fitting with its objective \citep{GVK120821842,AfsarB15,Oreja-RodriguezY06}. In our domain of software engineering, a quality organisational structure refers to better software, which is of more sustainable and measurable technical qualities \citep{Nielsen95}. For example, the Jet-Propulsion Laboratory at NASA can be said to have a high-quality organisational structure since it produces and maintains software which is virtually error-free\footnote{\url{https://www.fastcompany.com/28121/they-write-right-stuff}} through a combination of organisational as much as technical tools and approaches.

%\subsection{Community Smells in Action}
%
%As previously stated, our previous work on software organisational structure quality started from the industrial trenches, formalising and evaluating the notion of community smells \cite{jisaspecissue,archcomm}, that is, recurrent patterns of sub-optimal organisational behaviour that can be measured, e.g., using structural social network analysis \cite{sna}. In the scope of this article, we use an operationalisation of four community smells from our previous industrial work \cite{jisaspecissue,archcomm}, namely:
%
%	\begin{enumerate}
%		\item \emph{the Organisational Silo Effect:} siloed areas of the developer community that essentially do not communicate, except through one or two of their respective members;
%		\item \emph{the Black-cloud Effect:} information overload connected to lack of structured communication or cooperation/collaboration governance practices;
%		\item \emph{the Lone-wolf Effect:} unsanctioned or defiant contributors who carry out their work irrespective or irregardless of their peers (i.e., co-committing developers), their decisions and communication;
%		\item \emph{the Bottleneck or ``Radio-silence" Effect:} an instance of the problem of ``unique boundary spanner" \cite{sna} problem from social-networks analysis: one member interposes him/herself into every formal interaction across two or more sub-communities with little or no flexibility to introduce other parallel channels;
%	\end{enumerate}
%	
%Using the above community smells as indicators for sub-optimal quality, we then use \yoshi to find out which organisational structure types are less ``smelly" and which characteristics correspond to this higher-quality condition (if any).
%
%\subsection{Research Statement}\label{statement}
%
%Stemming from the above background and premises, our objectives in the scope of this paper are:
%
%\begin{framed} 
%	\textbf{Research Objective 1.} Offering an automatic tool that associates observable metrics in open-source communities with the organisational structure pattern reflected by those metrics.
%\end{framed}
%
%\begin{framed} 
%	\textbf{Research Objective 2.} Understanding how reliable the tool is and to what extent it can be exploited as a \emph{decision support system} for the management of open source systems.
%\end{framed}
%
%To address these objectives we firstly developed a tool, coined \textsc{\yoshi}, able to measure key organisational structure characteristics, e.g., the duration of a project team or the situatedness degree of a community \cite{situated}. These measured characteristics will match \emph{one or more} organisational structure types since the teams or communities in our sample may reflect characteristics of one type in one lifecycle phase, as well as other characteristics of a different type in other phases. Therefore, we defined:
%



%\begin{center}
%	\emph{Organisational Structure Pattern:\\}
%	$\phi = \sum \alpha(t_1)...\alpha(t_n)$;
%\end{center}
%
%where t$_{1...n}$ represent the organisational structure types from literature, while $\alpha$ represents the lifecycle-phase in which that type is prominent. Patterns of organisational structure types are identified by relying on the decision tree defined in our previous research \cite{specissue,icgseoss}. More in particular, the decision-tree encodes the set of relations (e.g., implication or mutual-exclusion) across primary indicators from Figure \ref{outline}. This set of relations forms, by definition, a partial-order function, i.e., a function that associates an ordering or sequencing to the elements of a set. The decision-tree (see Figure \ref{tree}) is a representation of this partial-order function and is to be visited top-to-bottom (most generic to most specific type) and right-to-left (most collocated to most dispersed type)\footnote{All relations and decision-tree functional demonstration by construction can be found online at \url{http://tinyurl.com/mzojyp2}}.
%%%%%%%%%% DECISION-TREE
%%%%%%%%%%\begin{figure}
%%%%%%%%%%	\begin{center}
%%%%%%%%%%		\includegraphics[scale=0.5]{Figures/treenew}
%%%%%%%%%%	\end{center}
%%%%%%%%%%	\caption{A decision-tree for organisational structures - dotted nodes identify types not currently implemented in \\yoshi.}\label{tree}
%%%%%%%%%%\end{figure}

%Once implemented the tool, we evaluated its capabilities in (i) correctly measuring community aspects characterising different open-source software communities, (ii) providing a meaningful overview of the community structure of a software system, and (iii) assisting project managers and developers in the identification of community-related issues and evaluate any corrective actions, e.g., by means of known, type-specific best practices.