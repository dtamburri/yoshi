%!TEX root = yoshi.tex
This section reports the implementation details behind \textsc{Yoshi}, as well as the details on the architecture and the functionalities currently implemented in the tool. As previously introduced in Sec. \ref{bg}, all operationalisations and detection patterns follow the Goal-Question-Metric approach \citep{gqm} and use empirically-defined thresholds from previous work \citep{titans}.

\subsection{The \yoshi Approach to Open-Source Community Design Patterns Detection: General Overview}

\yoshi is a social-networks analysis tool specifically designed for detecting open-source community types. The tool focuses on determining the levels of the previously-mentioned identifying characteristics, and combines specific version-control and committer activity data implemented in an information retrieval component (see bottom of Fig. \ref{yoshiarch}). For example, to determine how formal a community is, \yoshi looks at how many levels of control are assigned across repository contributors. Similarly, to evaluate engagement \yoshi looks both at the technical (e.g., commits, pull requests) and social or organisational (e.g., comments, new watchers) activities. 
\begin{figure}
	\begin{center}
		\includegraphics[scale=0.4]{Figures/treenew}
	\end{center}
	\caption{A decision-tree for organisational structures - dotted nodes identify types not currently implemented in \yoshi.}\label{tree}
\end{figure}

Once all characteristics are determined, \yoshi runs Algorithm \ref{alto} to determine the community type a given repository refers to. It is important to remark again that the tool allows to identify the existence of community types by looking at the existence of key community characteristics as well as their combination. For this reason, \emph{\yoshi identifies a community design pattern featuring multiple types within a certain repository}; several possible scenarios may exemplify this, e.g., multiple sub-teams working as different community types or the community works with different types at different phases in its organisational activity.  

To precisely distinguish the types existing in the observed organisation, \yoshi iteratively uses an algorithmic representation (see Sec. \ref{algoref}) of the decision-tree we previously evaluated in industry \citep{specissue}. The decision-tree in question (reported in Fig. \ref{tree}) encodes the set of relations (e.g., implication or mutual-exclusion) across primary indicators for community types from Table \ref{overview}. This set of relations forms, by definition, a partial-order function, i.e., a function that associates an ordering or sequencing to the elements of a set. The decision-tree (see Figure \ref{tree}) is a representation of this partial-order function and is to be visited top-to-bottom (most generic to most specific type) and right-to-left (most collocated to most dispersed type)\footnote{All relations and decision-tree functional demonstration by construction can be found online at \url{http://tinyurl.com/mzojyp2}}. \yoshi iterates on the decision-tree until no new community types are discovered over available data.
% DECISION-TREE
To exemplify the workings of the decision-tree, consider the tree-visit reflecting the identification of FNs in Fig. \ref{exampletree}.

\begin{figure}
	\begin{center}
		\includegraphics[scale=0.5]{Figures/Formal}
	\end{center}
	\caption{A decision-tree for organisational structures - \yoshi's visit to identify FNs.}\label{exampletree}
\end{figure}

Finally, \yoshi is able to visualise the software development network and its statistics over a world map, reporting statistics in *.csv format---this feature is implemented in \yoshi's own visualisation component.

\yoshi does not offer any insights over the technical qualities of the artefacts worked on by open-source communities under observation (e.g., software architecture, code, etc.), since these aspects are covered by several other state-of-the-art tools, e.g., SonarQube, CAST Software, or Titan~\citep{XiaoCK14}.

The above approach, can be easily replicated, generalised or further specialised at will. For example, the key organisational and socio-technical characteristics from the state-of-the-art~\citep{ossslr} may be observed through other, possibly more precise means (e.g., Natural-Language Processing \citep{manning1999fsn}, Neuro-Linguistic Programming \citep{Molzberger86}). Similarly, specific tools (or \yoshi forks) can be designed to address a more precise identification of one or two specific community types, e.g., focusing on Communities and Networks of Practice.

\subsection{\yoshi: Algorithmic Representation}\label{algoref}
	Algorithm \ref{alto} shows \textsc{Yoshi}'s measurement function \texttt{measure()} as applied to observable open-source communities. 
	To extract community types from observable data, Algorithm~\ref{alto} is executed as follows. 
	\begin{itemize}
	\item \yoshi establishes that there is in fact a high degree of community structure:\emph{ $\textbf{measure}(structure) == high $};
	\item \yoshi measures the indicators for the remaining five community characteristics:\\ \emph{$m[~~]\gets \textbf{measure}(GEO, ~LON, ~ENG, ~FOR, ~COH)$};
	\item \yoshi ascertains that characteristics are not null:\\
	\textbf{Assume}(\emph{$ m~!= \emptyset$});
	%\ALEXANDER{I do not see application of the thresholds here...}
	\item \yoshi applies empirical thresholds \citep{titans} and returns a certain community type if and only if its identifier has been found as ``Highest": \\
	\emph{$T_{x}\gets \textbf{True} \iff \textbf{Value}(m_{x}) = Highest \land \textbf{Attribute}(m_{x}) = T_{identifier}$};
	\end{itemize}

The 5 characteristics (besides community structure) computed by \yoshi (GEO, LON, ENG, FOR, COH in Algorithm \ref{alto}) are, intuitively: (GEO) geodispersion; (LON) longevity; (ENG) engagement; (FOR) formality; (COH) cohesion. The characteristics are operationalised in the tool as detailed in the following subsections.

\begin{algorithm}
\caption{\yoshi algorithm for community type detection using thresholds from previous work.}\label{alto}
\begin{algorithmic}
\If {$\textbf{measure}(structure)~==~high$}
\State $m[~~]\gets \textbf{measure}(GEO, ~LON, ~ENG, ~FOR, ~COH)$
\If {$ m~!= \emptyset$}
$Comm_{type}\gets [T_{1} ... T_{n}]$ 
\State \textbf{where:} 
\State $\textbf{forall}~~x = 1..8:$ 
\State \emph{$T_{x}\gets \textbf{True} \iff \textbf{Value}(m_{x}) = Highest \land \textbf{Attribute}(m_{x}) = T_{identifier}$} 
\EndIf
\EndIf
\State \textbf{return} $Comm_{type}$
\end{algorithmic}
\end{algorithm}

	\subsubsection{Community Structure}
	\label{subsec:communityStructure}
	As operationalised within \textsc{Yoshi}, this characteristic represents the ability to distinguish a non-trivial organisational and social structure within the observed set of people working on a project. Establishing this characteristic is paramount to identify any community type, since, by definition, organisational structures are sets of constant properties acting across social networks exhibiting community structure \citep{ossslr,NewGir04,newman03fast}. The success of open-source projects crucially depends on the voluntary contributions of a sufficiently large community of users. Apart from the size of the community, \emph{Structure} can be identified by looking at the evolution of structural features (e.g., connection density) of collaborations between community members. To analyse the social structure of communities, we collected data regarding user collaborations using API requests to each analysed repository. A definition of ``community" in the context of social networks analysis is a subnetwork whose intra-community edges are denser than the inter-community edges \citep{sna}. \textsc{Yoshi} computes a network of nodes representing community members and edges representing any particular social or organisational interaction between any two members. 
	\begin{framed}
		\noindent \textbf{Detection Strategy.} Two nodes are connected if at least one of the following conditions holds:
		
		\begin{enumerate}
			\item Common projects: two community members have at least one common repository to which they are contributing, except for the currently analysed repository;
			\item List of followers: between the considered community members exists either a ``is following" or ``follows" relation;
			\item Pull request interaction: we consider the connection between the pull request author and other community members that are participating on the pull request.
		\end{enumerate}
			
		Consequently, a structure exists if at least one of the above parameters is met within the standard analysis window of 3 months considered by \textsc{Yoshi}: the 3-month window is a common practice in organisations research \citep{TraagKD13} to observe organisational activity. \emph{Structure} is therefore a binary characteristic, to be possibly refined into a ratio or degree in the scope of future work around the tool.
		
	\end{framed}
	
	\begin{figure}
		\centering
		\includegraphics[scale=1.37]{Figures/geo.pdf}
		\caption{A geographical distribution map in \textsc{Yoshi}.}\label{geodisp}
	\end{figure}

	\subsubsection{Community Geodispersion} 
	As operationalised within \textsc{Yoshi}, this characteristic represents the cultural and geographical dispersion between community members. Establishing this characteristic is key to identifying either a \emph{Network of Practice} (high geodispersion) or a \emph{Community of Practice} (geodispersion low or none). For geographical dispersion (\texttt{GeoDispersion} class) \textsc{Yoshi} retrieves community members' specified location form their own profile and uses it to compute the median and standard deviation of the distance between them and to create a geographical distribution map \citep{geodisp} and, for cultural dispersion, \textsc{Yoshi} computes (\texttt{CulturalDispersion} class) Hofstede cultural distance metrics \citep{hofstede2010cultures} and their standard deviation.  
	
	\begin{framed}
		\noindent \textbf{Detection Strategy.} Because the location values do not provide geographical coordinates, \yoshi first uses location values to calculate Hofstede metrics and their standard deviation per-member and then exploits the \texttt{Google Geocoding }API to convert member addresses into geographic coordinates to compute the spherical distance between any two community members. Figure \ref{geodisp} shows an example geographical distribution map. The red dots on Figure \ref{geodisp} represent developers which are currently members of the targeted development community. As defined in our previous work \citep{titans}, \emph{Geodispersion} is computed as the sum of standard deviations of geographical and cultural difference (i.e., the sum of standard deviation of geographical distance across members and of all Hofstede metrics between all pairs of members across the community). From a mathematical perspective, \yoshi averages the variances of the two quantities and then takes the square root to get the average standard deviation; the tool also provides an option to choose for the Wolfram Alpha compute engine as an alternative to local JVM computations.
	\end{framed}
	
	\subsubsection{Community Longevity} 
	As operationalised within \textsc{Yoshi}, this characteristic represents the committer's longevity as a member of the observed community. Establishing this characteristic is essential to identifying \emph{Project Teams and Problem-Solving Communities} (low or time-bound longevity) or \emph{Workgroups} (high longevity). Committer longevity is a measure of how long one author remains part of the community. 
	
	\begin{framed}
		\noindent \textbf{Detection Strategy.} \textsc{Yoshi} iterates through the list of commits and extracts: (a) committing member; (b) creation date. These are then used for updating the data from \texttt{commiterStartDate} and \texttt{commiterStopDate} for every member. After all commits have been preprocessed, \textsc{Yoshi} computes the \emph{Longevity} value for each member as the difference between the two previously determined values.
	\end{framed}
	
	\subsubsection{Community Engagement} 
	As operationalised within \textsc{Yoshi}, this characteristic represents the participation levels across the analysed community, intended as the amount of time the member is actively participating with community-related actions. Establishing this characteristic is essential to identifying \emph{Informal Communities or Informal Networks} (high engagement). Also, evaluating this characteristic is essential for several community health reasons, for example, the case study presented by \citet{casoalexandra22} shows that developer engagement in their software projects is key in successful project development and has positive effects on user satisfaction. 
	
	\begin{framed}
		\noindent \textbf{Detection Strategy.} To establish engagement, \textsc{Yoshi} computes the following data about each community member: 
		
		\begin{enumerate}
			\item Total number of pull-request comments;
			\item Monthly distribution of total posted pull/commit comments---\yoshi extracts pull-request and commit comments posted by community members; 
			\item Number and list of repository active members (i.e., the members who committed at least once in the last 30 days)---\yoshi uses attributes values to measure the number of commit events initiated by users;
			\item Repository watcher members, i.e., third-parties who receive notifications of the activity across the community;
			\item Subscriptions, i.e., third-parties who get digests of commit activity across the community;
			\item Distribution of commits for each user;
			\item Distribution of collaborations on files---\yoshi examines the development activities of repository contributors to see if they were working together on common issues opened in the standard GitHub issue-tracker. 
		\end{enumerate}
	
		 Finally, \emph{Engagement} levels across the community are established as the member medians of the measurements above. 
	\end{framed}
	
	 As an illustrative example, we focus on the value which indicates how tightly project members collaborate on repository artefacts, that is, the number of community members that commit on common repository artefacts. 
	 \yoshi uses the \texttt{ContentsService} and \texttt{DataService} GitHub API classes to retrieve the repository file structure and associated commits. \yoshi then uses the \texttt{CommitService} GitHub API class that provides the \texttt{pageCommits} method for retrieving the history of commits for each file. In summary, \textsc{Yoshi} extracts authors for each commit and adds them to the set of file contributors. The result of these preprocessing operations is a \texttt{HashMap} which stores the sets of contributors for each repository artefact. This map allows us to determine the number of community members that commit on common repository artefacts. Each entry from this collection represents the set of connections that a repository user has established by common collaboration on repository items. 
	
	\subsubsection{Community Formality} 
	As operationalised within \textsc{Yoshi}, this characteristic represents the level of control (access privileges, milestones scheduling and regularity of contribution) exercised or self-imposed on the community. Establishing this characteristic is essential to identifying \emph{Formal Groups or Formal Networks} (high formality). Also, evaluating this is essential for several reasons. For example, as reported by \citet{Crowston2012}, open-source communities' approach to project milestones does not follow a common pattern. The results show that some projects have quite informal release schedules, following the pattern of releasing early and releasing often, whilst in other projects releases are more informal and come at an irregular rate \citep{aletaref17}. Depending on the formality type, different governance support schemes might apply (e.g., formal community types as opposed to informal ones \citep{ossslr}). 
	
	\begin{framed}
		\noindent \textbf{Detection Strategy.} \textsc{Yoshi} uses three measurements for formality:
		
		\begin{enumerate}
			\item Membership types---in general, Contributor and Collaborator are typical GitHub membership types and therefore, the levels of control assigned to repository members are +1 for contributors and +2 for collaborators (which have more privileges); 
			\item Project milestones---we used the Milestones GitHub API to extract the list of milestones associated to a project; 
			\item Project lifetime---this value is computed using the earliest and latest commits for a project, which are obtained using the API class \texttt{CommitService}. 
		\end{enumerate}
	
		\emph{Formality} is then determined as the mean membership type divided by the milestones per project-lifetime ratio.
	\end{framed}
	
The division above is mathematically grounded as follows. Since GitHub only allows contributor and collaborator as membership types, \yoshi associates a 1 to a contributor membership type and a 0 to collaborator membership type. Hence, the average number of contributors (i.e., the 1's) divided by the amount of work they have been able to carry out gives an indication of how well-structured their collaboration, co-location, co-operation works and hence, an indication of the \emph{formality}. Conversely, the more external collaborators (i.e., the 0's) there are, the less formal (i.e., closer to 0 formality) the structure will be. 
	
	For the sake of completeness, we only elaborate on how \textsc{Yoshi} detects member contributions and evaluates membership type, as well as defining the contributor \& collaborator subset. First, for each community member, \textsc{Yoshi} retrieves the set of repositories to which he/she has contributed. One of the following actions is considered a contribution: (a) a commit to a project's default branch or the gh-pages branch; (b) opening/closing an issue; (c) proposing a pull request. Second, \textsc{Yoshi} defines the set of repository members as the union of repository collaborators and repository contributors. The \texttt{Collaborator\-Service} GitHub API class allows retrieving the sets of collaborator-users for each of the considered repositories, whilst the \texttt{RepositoryService} GitHub API class that provides us with set of contributor-users. 
	
	\subsubsection{Community Cohesion} 
	As operationalised within \textsc{Yoshi}, this characteristic represents the level to which there are tight social or organisational or technical relations among members of the community. 
	It is worth noting that we inherit the term ``community cohesion'', its definition, and rationale from the state of the art in working-groups~\citep{MoodySR,HungTrans,GirardoCyber}; the definition in question includes a strong connotation of community cohesion \citep{MoodySR,HungTrans} which is associated to low cognitive distance among members and hence, high expertise overlap \citep{NooteboomCognitive}.

	\begin{framed}
		\noindent \textbf{Detection Strategy.} For each community member \yoshi determines the number of followed or following users, which are also members of the same community and share significant expertise overlap (i.e., common programming language skills). Using the  \texttt{UserService} API class \textsc{Yoshi} extracts the list of users that a repository member is following and the list of users that are following his activity. From these two lists \textsc{Yoshi} excludes the users that were not members of the currently analysed repository. Applying the same preprocessing operations for all repository members, the result is a  \texttt{Map$<$User, Integer$>$} representing a mapping between users and their number of team-followers. \emph{Cohesion} is computed as the community median of the composite measurements above.
	\end{framed}
For the sake of completeness, we elaborate on how \textsc{Yoshi} detects common skills. In general, GitHub user profile attributes do not include their technical skills. In substitution, \textsc{Yoshi} uses the  \texttt{RepositoryService} API class to retrieve the repositories to which a user has made contributions. Repository entities include the main programming language attribute value which allows us to compute a set of programming languages from the list of repositories. For each repository, we obtain a map  \texttt{Map$<$Contributor, Set$<$String$>>$} representing the mapping between repository members and a crude list of their acquired technical skills. These values are used as a basis for determining followers with common skills.
	Using this data for each repository member we compute the list of projects to which they have contributed and determine the number of projects to which members of the current project community have collaborated. Finally, the  \texttt{WordFrequency} class in \textsc{Yoshi} uses the content of messages exchanged between community members including the commit messages and pull requests messages to determine the most frequently used words and categorise them into skills using a taxonomy of software engineering skills of our own design \citep{edustudy}. 
	
\begin{figure}
\centering
	\includegraphics[scale=0.4]{Figures/yoshiarch.pdf}
	\caption{\yoshi high-level architecture.}\label{yoshiarch}
\end{figure} 
	
\subsection{\textsc{Yoshi}---Architecture}
Figure \ref{yoshiarch} shows a basic view of \textsc{Yoshi}'s software architecture using a basic input-output control flow diagram \citep{BCK98}. \textsc{Yoshi} has a modular architecture arranged in three components.

First, an information retrieval component (bottom part of Fig.~\ref{yoshiarch}) is responsible for retrieving data with which \textsc{Yoshi} can perform its functions. The component automates the retrieval of data from public repositories of projects hosted on Github, using GET requests from GitHub Archive to obtain publicly available data. The retrieval component extracts data from two data sources: source code management systems and issue trackers. First, GitHub Archive records the public GitHub timeline, archives it, and makes it easily accessible for further analysis; the archive dataset can be accessed via Google BigQuery. This data is used to compute attributes' values related to the software development process and study targeted open-source software development communities. 

Second, the processing component is responsible for evaluating metrics using data available from the retrieval component and to enable community detection (see Algorithm 1). The component uses: (a) Gephi---a Java library which provides useful and efficient network visualisation and exploration techniques; (b) Google Geocoding API---used for converting the addresses of repositories members into geographic coordinates, which is used to calculate distances; (c) a direct way to access services via an HTTP request; (d) an implementation of Algorithm \ref{alto}.

Third, the visualisation component uses data computed by the processing component to create graphical representations of community members' geographical distribution. This component is able to export images and Comma-Separated Values (CSV) files for the produced representations. Finally, the current implementation of \textsc{Yoshi} also supports reporting of computed characteristics, their composing metrics, their values and the resulting community design patterns. Furthermore, \textsc{Yoshi} was designed in a modular, service-based architecture, so as to be easily extended with third-party tools (e.g., sentiment analysis~\citep{Novielli:Calefato:Lanubile,Jongeling2017}, natural-language processing~\citep{Arnaoudova:et:al}). 	
